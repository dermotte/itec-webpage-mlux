# Game Engineering

The goal of this course is to introduce students to computer game engineering. This is
not necessarily a technical issue especially as game engineering involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing. 

After this course students should be able to understand the concepts and roles within a game project with the 
knowledge how a game project can be set up and run, as well as detailed knowledge on particular processes within the 
project.

The practical course associated with the lecture is taking place as a game jam. Find more information on that on the [winterjam website](http://itec.aau.at/gamejam/).

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/10uisgSUasYtfPSkOCBCN3ZXZxNgNgN3PY_pd0nsm40Y/edit?usp=sharing)
2. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/1aAhK5katRqKCYVlsvCYGRMZXyI9Jic583_u12sxLwIo/edit?usp=sharing)
3. Game Projects: Documents and Pitches ~ slides: see above.
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/1QUgnsKtcdOSzWDK_qC86a9yV2TSsK5E9eOdMT-kk79M/edit?usp=sharing)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1vCbru3UG5VaK1JftJunOCyUHkiDzViVgMeT_k7Qw8w8/edit?usp=sharing)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1hwg0RjypVa1DAFzLxBtIKunhUY4TVIl8E8zhdWvkDJQ/edit?usp=sharing)
7. Playtests ~ [slides](https://docs.google.com/presentation/d/1XIDETvz5c5KcP7uwZcV1QF_kql_tfsLiYGHV8eRB8sg/edit?usp=sharing)
6. Hands On with Game Engines ~ [slides](https://docs.google.com/presentation/d/114CiFS4drFa7deCh8eLC-i7vToOJBImw7nPTpA2FMNc/edit?usp=sharing)
8. Sound & Interactive Music ~ [slides](https://docs.google.com/presentation/d/1fw2H-rTru3wBYa95d4IC4N3NXFr2uQy9FaGzxgXe4ic/edit?usp=sharing)

## Modalities 

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The exam will be in written 
form, take 50 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://drive.google.com/open?id=1PF0CIe6nT5-dmRtOhRKg5nrlPFZfr6ikoH1xQHE22iU). 

## Links

- [Hour of Code](https://code.org)
- [Scratch](https://mit.scratch.edu)
- [Google Blockly](https://developers.google.com/blockly/)