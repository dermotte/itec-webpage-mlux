# Advanced Topics in Computer Graphics

This course will dive into specific topics in the broad area of computer graphics. We will focus on (i) procedurally generated content (PCG) and (ii) AR/VR. The course will require a significant amount of practical work, so programming knowledge is required. We will implement standard algorithms for PCG in computer games and put them to use in a VR environment. The web page will be updated continuously. Grading is based on (i) mandatory assignments, (ii) a final project including a video demo and a written report, and (iii) a final exam.

Find the lecture schedule at [the AAU Campus System](https://campus.aau.at/studium/course/112938). All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below).

## Topics

* Introduction to procedurally generated content ~ [slides](https://docs.google.com/presentation/d/1NTLubtBW9exh7QScUAW0gK6VnXBbucQ-GlNnP7LNFgw/edit?usp=sharing)
* Overview on PCG algorithms and pseudo-random number generators ~ [slides](https://docs.google.com/presentation/d/1bmOxQziYmdsC1Jk1vyvpvLiRDhqC8cSxj8SMu7o40mw/edit?usp=sharing)
* Noise functions ~ [slides](https://docs.google.com/presentation/d/11f1uWSBi0ogJ5k44LI5AIcSQ7v7uShxWQPQVegIMkls/edit?usp=sharing)
* Labyrinths ~ [slides](https://docs.google.com/presentation/d/15n7AcRAjdD9bdQ8X63t-LrSmBLrWqPrc78DMAQlPRrA/edit?usp=sharing)
* L-Systems & Erosion ~ [slides](https://docs.google.com/presentation/d/1_EzermGVE1ufQwSKXt-TmxCR6mBIPQ7gjykM7EKOgKk/edit?usp=sharing)
* Puzzles: Sudoku ~ [slides](https://docs.google.com/presentation/d/1WKnac5C2MiF46FTsVr7bIz_WPtrYJv5mikWiZUD6tKU/edit?usp=sharing)
* Introduction to VR ~ [slides](https://docs.google.com/presentation/d/1vYp9mVa3-mXRncrO9xgYyoiur_1HhCaaDgwRQELLsT4/edit?usp=sharing)
* Introduction to WebXR ~ [slides](https://docs.google.com/presentation/d/1OLGI0fAtyIrnS1OH4CEue1Piyr8_aFc8tEIL-H7ujPM/edit?usp=sharing)

Find the source code used in the lecture on Github: https://github.com/dermotte/AdvancedTopicsCG-ST2022

## Exercises (Mandatory Assignments)

Please upload the exercises (no .exe please, stick to .js, or export to the web) to Moodle until the end of the announced day.

| ID | Title           | Slide                                                                                                       | Deadline |
|:---|:----------------|:------------------------------------------------------------------------------------------------------------|:---|
| 01 | 3D Game of Life | [#32](https://docs.google.com/presentation/d/1NTLubtBW9exh7QScUAW0gK6VnXBbucQ-GlNnP7LNFgw/edit?usp=sharing) | Dec 20, 2022|
| 02 | Gaussian RNG | [#40](https://docs.google.com/presentation/d/1bmOxQziYmdsC1Jk1vyvpvLiRDhqC8cSxj8SMu7o40mw/edit?usp=sharing)    | Dec 20, 2022|
| 03 | Perlin Noise Shader | [#45](https://docs.google.com/presentation/d/11f1uWSBi0ogJ5k44LI5AIcSQ7v7uShxWQPQVegIMkls/edit?usp=sharing) | Dec 20, 2022|
| 04 | 3D Maze | [#55](https://docs.google.com/presentation/d/15n7AcRAjdD9bdQ8X63t-LrSmBLrWqPrc78DMAQlPRrA/edit?usp=sharing) | Jan 10, 2023 |

## Final Project

The final project needs to include (i) procedurally generated content based on an algorithm implementation of your choice and (ii) needs to be runnable on a VR headset. An easy choice is to prepare a WebXR export or use of a framework with WebXR support. For submission you need to upload the following files in a single ZIP file.

1. Your work as source code or a link to Github, Bitbucket, etc.
2. Your result as and executable for the web (test before upload!)
3. Your PDF presentation slides showing your work and summarizing the report.
4. A formatted PDF work report of ~ 1000 words detailing what you did and the lessons learned and obstacles encountered.

Please note term paper format, code of conduct, and plagiarism policies apply as detailed below.

### Paper Format
- Orderly fulfillment of formal requirements:
    * A paper follows the [Chicago Manual of Style](https://www.chicagomanualofstyle.org/) - use the [Overleaf template](https://www.overleaf.com/latex/examples/the-chicago-citation-style-with-biblatex/pdqqrmwtdqpc) if you want a strict implementation.
    * A paper is coherently formatted
    * A paper has a bibliography referencing all employed sources, including but not limited to online videos, web pages, and code repositories
- Adequate reliance on research literature:
    * Selection of citations
    * Provision of a well-formatted bibliography
-	Reliance on theoretical resources
-	Self-made observations on the discussed primary resource
-	Critical reflection on self-made observations


### Code of Conduct and Plagiarism Policies

The [University of California Berkely](https://sa.berkeley.edu/conduct/integrity/definition) offers the following definition:

```md
Plagiarism is defined as use of intellectual material produced by another person without acknowledging its source, for example:
* Wholesale copying of passages from works of others into your homework, essay, term paper, or dissertation without acknowledgment.
* Use of the views, opinions, or insights of another without acknowledgment.
* Paraphrasing of another person’s characteristic or original phraseology, metaphor, or other literary device without acknowledgment.
```

For computer science classes this includes source code, video tutorials, and web pages used to create software for the course. It is mandatory to check for license compatibility, like (i) can it be used, (ii) how to attribute, and (iii) what license is implied by including this source. It is also mandatory to cite the sources in the report. Failing to do that counts as plagiarism.

Note that the [Code of Conduct of the AAU](https://www.aau.at/en/research/research-profile/good-academic-practice/) applies. Students caught plagiarising or violation of the code of conduct will be removed from class immediately.

## Links

* [p5js](https://p5js.org/) - Simple JavaScript framework with an online editor, used in the course for examples
* [Babylon.js](https://www.babylonjs.com/) - Extensive Javascript 3D library with VR support
