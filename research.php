<!--- ------------------- MAIN ------------------- -->
<blockquote><p>Yes. In 1917, when Albert Einstein established the theoretic foundation for the laser in his paper "Zur
        Quantentheorie der Strahlung", his fondest hope was that the resultant device be bitchin'.</p>
    <footer>Sheldon Cooper, Big Bang Theory, Episode "The Lunar Excitation"</footer>
</blockquote> <h2>User Intentions</h2>
<div class="row">
    <div class="col-md-6"><p class="text-justify"> User intentions in multimedia is a novel and promising field. It
            deals with the motivation <i>why</i> images and videos are taken, viewed and searched for. Research on user
            intentions includes qualitative and quantitative studies on (i) motivations for search and production of
            multimedia content, (ii) goals that users want to achieve by creating and searching multimedia content, and
            (iii) log and data analysis to find traces of intentions in search and production. A list of selected
            publications would be: </p>
        <ul>
            <li>Riegler, M., Larson, M., Lux, M., & Kofler, C. (2014, November). How'How'Reflects What's What:
                Content-based Exploitation of How Users Frame Social Images. In Proceedings of the ACM International
                Conference on Multimedia (pp. 397-406). ACM. <a
                    href="http://dx.doi.org/10.1145/2647868.2654894">online</a></li>
            <li> M. Lux, D. Xhura, A. Kopper (2014) User Intentions in Digital Photo Production: A Test Data Set.
                MultiMedia Modeling. Springer International Publishing, 2014. <a
                    href="http://link.springer.com/chapter/10.1007/978-3-319-04114-8_15">online</a></li>
            <li> M. Riegler, M. Lux, C. Kofler (2013) Frame the Crowd: Global Visual Features Labeling boosted with
                Crowdsourcing Information. Proceedings of the MediaEval 2013 Multimedia Benchmark Workshop, Barcelona,
                Spain, <a href="http://ceur-ws.org/Vol-1043/mediaeval2013_submission_56.pdf">PDF</a></li>
            <li> M. Lux , J. Huber (2012) Why did you record this video? An Exploratory Study on User Intentions for
                Video Production. WIAMIS 2012 <a href="http://dx.doi.org/10.1109/WIAMIS.2012.6226758" class="urlextern"
                                                 title="http://dx.doi.org/10.1109/WIAMIS.2012.6226758" rel="nofollow">IEEE</a>
                <a href="http://www.itec.uni-klu.ac.at/~mlux/files/papers/mlux-wiamis-2012-preprint.pdf"
                   class="urlextern"
                   title="http://www.itec.uni-klu.ac.at/~mlux/files/papers/mlux-wiamis-2012-preprint.pdf"
                   rel="nofollow">preprint</a>, <a
                    href="http://www.slideshare.net/dermotte/why-did-you-record-this-video" class="urlextern"
                    title="http://www.slideshare.net/dermotte/why-did-you-record-this-video" rel="nofollow">slides</a>
            </li>
            <li> C. Lagger, M. Lux, O. Marques, (2012) What Makes People Watch Online Videos: An Exploratory Study.
                Short article teaser published in ACM CIE <a
                    href="http://cie.acm.org/articles/adaptive-video-retrieval-system-based-recent-studies-user-intentions-while-watching-videos-online/">online</a>.
            </li>
            <li> C. Lagger, M. Lux, O. Marques, (2012) What Makes People Watch Online Videos: An Exploratory Study.
                accepted for publication in ACM CIE <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/files/papers/CiE_lagger_lux_preprint.pdf">preprint</a>
            </li>
            <li> M. Lux, M. Taschwer, O. Marques (2012) A Closer Look at Photographers' Intentions: a Test Dataset, in
                Proceedings of the International ACM Workshop on Crowdsourcing for Multimedia held in conjunction with
                ACM Multimedia 2012, Oct 29 - Nov 2 2012, Nara, Japan <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/cmm4061-lux.pdf">preprint</a>, <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/CrowdMM-Poster%20B0.pdf">poster</a>.
            </li>
            <li> M. Kogler, M. Lux, O. Marques (2011) Adaptive Visual Information Retrieval by changing visual
                vocabulary sizes in context of user intentions. MMWeb 2011 <a
                    href="http://doi.ieeecomputersociety.org/10.1109/MMWeb.2011.13" class="urlextern"
                    title="http://doi.ieeecomputersociety.org/10.1109/MMWeb.2011.13" rel="nofollow">IEEE</a>.
            </li>
            <li> C. Lagger, M. Lux, O. Marques (2011) Which video do you want to watch now? Development of a
                Prototypical Intention-based Interface for Video Retrieval. MMWeb 2011, <a
                    href="http://doi.ieeecomputersociety.org/10.1109/MMWeb.2011.15" class="urlextern"
                    title="http://doi.ieeecomputersociety.org/10.1109/MMWeb.2011.15" rel="nofollow">IEEE</a>.
            </li>
            <li> M. Lux, C. Kofler, O. Marques (2010) A classification scheme for user intentions in image search. CHI
                2010 <a
                    href="http://dl.acm.org/citation.cfm?id=1753846.1754078&amp;coll=DL&amp;dl=GUIDE&amp;CFID=55695921&amp;CFTOKEN=54470683"
                    class="urlextern"
                    title="http://dl.acm.org/citation.cfm?id=1753846.1754078&amp;coll=DL&amp;dl=GUIDE&amp;CFID=55695921&amp;CFTOKEN=54470683"
                    rel="nofollow">ACM page</a></li>
            <li> M. Lux, M. Kogler, M. del Fabro (2010) Why did you take this photo: a study on user intentions in
                digital photo productions. SAPMIA 2010 <a
                    href="http://dl.acm.org/citation.cfm?id=1878061.1878075&amp;coll=DL&amp;dl=GUIDE&amp;CFID=55695921&amp;CFTOKEN=54470683"
                    class="urlextern"
                    title="http://dl.acm.org/citation.cfm?id=1878061.1878075&amp;coll=DL&amp;dl=GUIDE&amp;CFID=55695921&amp;CFTOKEN=54470683"
                    rel="nofollow">ACM page</a></li>
            <li> C. Kofler, M. Lux (2009) Dynamic presentation adaptation based on user intent classification. ACM
                Multimedia 2009 <a
                    href="http://dl.acm.org/citation.cfm?id=1631272.1631526&amp;coll=DL&amp;dl=GUIDE&amp;CFID=55695921&amp;CFTOKEN=54470683"
                    class="urlextern"
                    title="http://dl.acm.org/citation.cfm?id=1631272.1631526&amp;coll=DL&amp;dl=GUIDE&amp;CFID=55695921&amp;CFTOKEN=54470683"
                    rel="nofollow">ACM page</a></li>
        </ul>
        <p> We further released a test data set of 1,309 images along with the intentions of the photographers acquired
            in a large survey. More information and the actual data is available here: <a
                href="?id=photointentionsdata">photointentionsdata</a></p></div>
    <div class="col-md-6"><p class="text-center">
            <iframe src="https://docs.google.com/presentation/d/1FnnT1ZK02tPH7TiR1TZ2YjovHu65CVXjFnimpC0yMCw/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<!--        <blockquote><h3>Definition of context</h3> Context is any information that can be used to characterize the
            situation of an entity. An entity is a person, place, or object that is considered relevant to the
            interaction between a user and an application, including the user and applications themselves.
            <footer>G. Abowd et al., "Towards a better understanding of context and context-awareness"</footer>
        </blockquote>
-->        <blockquote><h3>Definition of intention</h3> <i>noun</i><br/> (1) thing intended; an aim or plan<br/>
            (2) Medicine the healing process of a wound<br/> (3) (intentions) Logic conceptions formed by directing the
            mind towards an object<br/>
            <footer>Oxford Dictionary</footer>
        </blockquote>
        </p> </div>
</div>
<!--<div class="row">
    <div class="col-md-12">
        <iframe src="https://www.slideshare.net/slideshow/embed_code/key/clhQoybYaBWRo" width="476" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
    </div>
</div>
--><h2>Content Based Image Retrieval with LIRe</h2>
<div class="col-md-8"><p class="text-justify"> LIRE (Lucene Image Retrieval) is an open source library for content based
        image retrieval. Besides providing multiple common and state of the art retrieval mechanisms it allows for easy
        use on multiple platforms. LIRE is actively used for research, teaching and commercial applications. Due to its
        modular nature it can be used on process level (e.g. index images and search) as well as on image feature level.
        Developers and researchers can easily extend and modify LIRe to adapt it to their needs. In 2013 Oge Marques and
        me published a book on LIRE meant to give an introduction to content based image retrieval as well. Look out for
        the book at <a href="http://www.morganclaypool.com/doi/abs/10.2200/S00468ED1V01Y201301ICR025">Morgan &amp;
            Claypool</a> or at <a
            href="http://www.amazon.com/Visual-Information-Retrieval-using-Java-ebook/dp/B00CDGMPR0/ref=sr_1_1?ie=UTF8&qid=1421229960&sr=8-1">amazon.com</a>.
    </p>

    <p class="text-justify">In addition to the LIRE Java library there's also a plug-in for Solr, the Apache search
        server based on Lucene. The most impressive use of the LIRE Solr plugin is over at the United Nations for their
        global brand data base. A peer reviewed demo paper explaining the context and approach can be found <a
            href="http://link.springer.com/chapter/10.1007/978-3-319-04117-9_39">here</a>. A live demo is online
        available at <a href="http://demo-itec.uni-klu.ac.at/liredemo/">http://demo-itec.uni-klu.ac.at/liredemo/</a>.
    </p>
    <ul>
        <li>M. Lux, S.A. Chatzichristofis (2008) LIRE: Lucene Image Retrieval: An extensible Java CBIR Library. ACM
            Multimedia 2008. <a href="http://dl.acm.org/authorize?026312">Download the paper.</a></li>
        <li>Lux, Mathias, and Oge Marques. "Visual information retrieval using java and lire." Synthesis Lectures on
            Information Concepts, Retrieval, and Services 5.1 (2013): 1-112. Buy the book at <a
                href="http://www.morganclaypool.com/doi/abs/10.2200/S00468ED1V01Y201301ICR025">Morgan &amp; Claypool</a>
            or at <a
                href="http://www.amazon.com/Visual-Information-Retrieval-using-Java-ebook/dp/B00CDGMPR0/ref=sr_1_1?ie=UTF8&qid=1421229960&sr=8-1">amazon.com</a>
        </li>
        <li>Lux, Mathias, and Glenn Macstravic. "The LIRE Request Handler: A Solr Plug-In for Large Scale Content Based
            Image Retrieval." MultiMedia Modeling. Springer International Publishing, 2014. <a
                href="http://link.springer.com/chapter/10.1007/978-3-319-04117-9_39">Download the paper.</a></li>
        <li> Website: <a href="http://www.lire-project.net/">The LIRE Project</a></li>
    </ul>
</div>
<div class="col-md-4"><p class="text-center"><img src="images/screenshot-liredemo.jpg"
                                                  class="img-thumbnail img-responsive"
                                                  style="margin: 2px; max-height: 400px; width: auto"></p></div>