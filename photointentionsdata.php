    <!--- ------------------- MAIN ------------------- -->


        <h1 class="sectionedit1" id="test_data_set">Test Data Set</h1>

        <div class="level1">

            <p>
                We aquired a data set of 1,309 photos and did two surveys to find out (i) why they were taken and (ii)
                to rate the readability and reliability of survey answers. Our data set is available for research
                purposes.
            </p>

            <p>
                The photos have been collected in June to September 2011. Photos were selected randomly from <a
                    href="http://www.flickr.com" rel="external">flickr.com</a>
                employing an RSS feed featuring recent additions to flickr.com. For each of the identified photos we
                posted a comment to invite the phototographer and owner of the published photo to support a scientific
                project by participtating in a survey. The survey featuread a small questionaire, (i) asking for
                permission to use the data in a scientific and non-commercial context and (ii) surveying possible
                intentions of the photographer. Besides a free text question on the why the photographer took the photo,
                we asked to rate the following statements based on a five point Likert scale from <em>strongly
                disagree</em> to <em>strongly agree</em>. The selection of the statement was based on the results of M.
                “Why did you take this photo: a study on user intentions in digital
                photo productions” (M. Lux, M. Kogler, and M. del Fabro., in Proceedings of the 2010 ACM Workshop on
                Social, adaptive and personalized
                multimedia interaction and access, SAPMIA &#039;10).
            </p>

            <p>
                <a href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/Intentions-Data-Set-2012-10-29.zip"
                   class="urlextern"

                        >DOWNLOAD DATA SET</a> (~117 <abbr>MB</abbr>)
            </p>

            <p>
                A short description of the data set is available in the paper “<em>A Closer Look at Photographers’
                Intentions: a Test Dataset</em>” (M. Lux, M. Taschwer &amp; O. Marques, in Proceedings of the
                International ACM Workshop on Crowdsourcing for Multimedia held in conjunction with ACM Multimedia 2012,
                Oct 29 - Nov 2 2012, Nara, Japan – <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/cmm4061-lux.pdf" class="urlextern"
                    >download
                PDF</a>). Please make sure to cite this paper if you use the data set. The poster presented there can be
                found <a href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/CrowdMM-Poster%20B0.pdf"
                         class="urlextern"

                    >here</a>
            </p>

        </div>
        <!-- EDIT1 SECTION "Test Data Set" [1-2043] -->
        <h1 class="sectionedit2" id="sample_application">Sample Application</h1>

        <div class="level1">

            <p>
                We submitted a sample application that used a classifier trained on the data set to the ACM MM 2012
                Grand Challenge: “<em>Classification of Photos based on Good Feelings</em>” (M. Lux, M. Taschwer &amp;
                O. Marques, in Proceedings of the International conference ACM ACM Multimedia 2012, Oct 29 - Nov 2 2012,
                Nara, Japan – <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/acm-mm-gc-2012-lux-taschwer.pdf"
                    class="urlextern"

                    >download PDF</a>)
            </p>

            <p>
                A presentation showing the application in action and describing part of the underlying principles given
                at ACM MM 2012 is <a href="http://youtu.be/omk3jz4i4Do" class="urlextern"
                    >available on YouTube</a>.
            </p>

            <p>
                The actual application can be <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/ImageSearchApp-2012-10-17.7z"
                    class="urlextern"

                    >downloaded here</a>.
            </p>

            <p>
                <a href="http://www.itec.uni-klu.ac.at/~mlux/files/acmmm2012/screenshots.jpg" class="media"
                        ><img
                        src="images/screenshots_intentions_small.jpg"
                        width="400"/></a>
            </p>

        </div>

