    <!--- ------------------- MAIN ------------------- -->
        <h1 id="useful_software">Useful Software</h1>

        <div>

            <p>
                This page points out some tool useful for several tasks given in diverse courses. Tools give here are
                free (at least for personal or academic use).
            </p>

        </div>
        <h2 id="windows">Windows</h2>

        <div class="level2">

        </div>
        <h3 id="archiving">Archiving</h3>

        <div class="level3">
            <ul>
                <li>
                    <div><a href="http://www.7-zip.org/" re="external"
                            >7-Zip</a>: Archiver &amp; Compression utility (ZIP, BZ2, GZ, TAR,
                        7z, …) on Windows
                    </div>
                </li>
                <li>
                    <div><a href="http://www.izarc.org/" re="external"
                            >IZArc</a>: Archiver &amp; compression utility (ZIP, Bz2, GZ, TAR,
                        7z, …) on Windows
                    </div>
                </li>
            </ul>

        </div>
        <!-- EDIT3 SECTION "Archiving" [198-434] -->
        <h3 id="office_pdf_media">Office, PDF &amp; Media</h3>

        <div class="level3">
            <ul>
                <li>
                    <div><a href="http://www.pdfforge.org/products/pdfcreator/" re="external"

                            >PDFCreator</a>: Creation of PDF documents on Windows (installs a
                        printer useable from all programs)
                    </div>
                </li>
                <li>
                    <div><a href="http://www.libreoffice.org/" re="external"
                            >LibreOffice</a>: Document
                        suite with integrated PDF support.
                    </div>
                </li>
                <li>
                    <div><a href="http://www.gimp.org/downloads/" re="external"
                            >Gimp</a>: Image
                        manipulation and photo editing.
                    </div>
                </li>
                <li>
                    <div><a href="http://audacity.sourceforge.net/" re="external"
                            >Audacity</a>: Audio
                        editing
                    </div>
                </li>
            </ul>

        </div>
        <!-- EDIT4 SECTION "Office, PDF & Media" [435-980] -->
        <h3 id="development">Development</h3>

        <div class="level3">
            <ul>
                <li>
                    <div><a href="http://www.jetbrains.com/idea/index.html" re="external"
                            >JetBrains
                        IDEA</a>: Great free Java &amp; JavaScript IDE (Community Edition) for Win, Linux &amp; Mac
                    </div>
                </li>
                <li>
                    <div><a href="http://www.eclipse.org/" re="external"
                            >Eclipse</a>: Open Source Java, JavaScript &amp; C++ IDE for
                        Windows &amp; Linux
                    </div>
                </li>
                <li>
                    <div><a href="http://www.netbeans.org/" re="external"
                            >NetBeans</a>: Open Source Java,
                        JavaScript, Ruby &amp; C++ IDE for Windows &amp; Linux
                    </div>
                </li>
                <li>
                    <div><a href="http://notepad-plus.sourceforge.net/" re="external"
                            >Notepad++</a>: Open
                        Source Windows text editor with lots of functions and syntax highlighting for many different
                        languages.
                    </div>
                </li>
            </ul>

        </div>
