# Seminar Distributed MM Systems (2017)
In this seminar on distributed multimedia systems your task is to pick a specific topic from this field and study,
assess, summarize and discuss related literature in your own paper. All in all your seminar thesis should give an
overview on the state of the art for unaware readers. Note at that point that your seminar paper is assessed as a
scientific paper.

# Schedule

1. Di, 07.03. 2017 - Initial meeting &amp; Topics
1. Di, 21.03. 2017 - LaTeX, Template, Research Q&A
1. Di, 16.05. 2017 - Intermediate presentations
1. Di, 27.06. 2017 - Final presentations


# Modalities

* Taking part in the course is mandatory. If you cannot be, there send an email to the lecturer in time. Missing courses results in failing the course.
* Start, intermediate and final presentations are mandatory. Missing to give either of them results in failing the course.
* The final paper has to be sent in __middle of June 2017__ latest. If you can't make the date discuss that with the lecturer in time.

# Your paper
Your task is to find work related to the the topic you select, read and assess it, put it into context and describe 
relations, benefit etc. This is also called a comparative study. 

Your paper is a scientific article. It has to match scientific quality in terms of citations, argumentation, facts, readability, and so on.
Your paper has to follow the [ACM SIG Proceeding style](http://www.acm.org/sigs/publications/proceedings-templates").
The final paper has to have 5000 words, which is around 6 pages in the given style. Make sure you use LaTeX for writing. There is an [example on Overleaf](https://www.overleaf.com/read/mgwxzcgvqcbs).

# Presentations
Each of the participants has to give two presentations.

## Intermediate presentation
Goal of this presentation is to show the topic to your colleagues and to show your current status of the seminar paper. At this point you should have read and understood your starting papers and have gotten a feeling for the topic as a whole. You don't need to have significant text, but I expect you to have found and investigates up to 5 related papers. The presentation is scheduled to last 240 seconds in [PechaKucha](https://en.wikipedia.org/wiki/PechaKucha)-like style, so each of your 12 slides is up on the wall for 20 seconds. Make sure you have got your slide setup prepared for that.

## Final presentation
The final presentation should take 10 minutes (no more and no less) and should summarize your work done in the paper.

# Topics

Bring along yourself or choose a subtopic from the repository of [Awesome Deep Learning Papers](https://github.com/terryum/awesome-deep-learning-papers).

1. Hofmeister W. - CNNs
1. Krumpholz M. - Deep Neural Networks are Easily Fooled
1. Steiner M. - [Image: Segmentation / Object Detection](https://github.com/terryum/awesome-deep-learning-papers#image-segmentation--object-detection) 
1. Steiner J. - [Unsupervised / Generative Models](https://github.com/terryum/awesome-deep-learning-papers#unsupervised--generative-models)