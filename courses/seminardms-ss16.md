# Seminar Distributed MM Systems (2016)
In this seminar on distributed multimedia systems your task is to pick a specific topic from this field and study,
assess, summarize and discuss related literature in your own paper. All in all your seminar thesis should give an
overview on the state of the art for unaware readers. Note at that point that your seminar paper is assessed as a
scientific paper.

# Schedule

1. Di, 01.03. 2016 - Initial meeting &amp; Topics
1. Di, 08.03. 2016 - LaTeX, Q&A (optional)
1. Di, 15.03. 2016 - Start presentation
1. Di, 10.05. 2016 - Intermediate presentations
1. Di, 07.06. 2016 - Final presentations I (starts s.t. at 10.oo am)
1. ~~ Di, 14.06. 2016 - Final presentations II ~~

# Modalities

* Taking part in the course is mandatory. If you cannot be, there send an email to the lecturer in time. Missing courses results in failing the course.
* Start, intermediate and final presentations are mandatory. Missing to give either of them results in failing the course.
* The final paper has to be sent in __middle of June 2016__ latest. If you can't make the date discuss that with the lecturer in time.

# Your paper
Your task is to find work related to the the topic you select, read and assess it, put it into context and describe 
relations, benefit etc. This is also called a comparative study. 

Your paper is a scientific article. It has to match scientific quality in terms of citations, argumentation, facts, readability, and so on.
Your paper has to follow the [ACM SIG Proceeding style](http://www.acm.org/sigs/publications/proceedings-templates").
The final paper has to have 5000 words, which is around 6 pages in the given style. Make sure you use LaTeX for writing.

# Presentations
Each of the participants has to give three presentations.

## Start presentation
Goal of this presentation is to show the topic to your colleagues. At this point you should have read and understood your
starting paper and have gotten a feeling for the topic as a whole. The presentation is scheduled to last 180 seconds in
[PechaKucha](https://en.wikipedia.org/wiki/PechaKucha)-like style, so each of your 9 slides is up on the wall for 20 seconds.
Make sure you have got your slide setup prepared for that.

## Intermediate presentation
Goal of the presentation is to show your current status of the seminar paper. At this time point you should have an
outline of your work and a collection of papers, which makes up your basis. You don't need to have significant text, but
I expect you to have found and read 5-10 related papers.

In this presentation you have 6 minutes time to show your current status and to outline your steps to completion including
a paper structure and a timeline.

## Final presentation
The final presentation should take 10 minutes (no more and no less) and should summarize your work done in the paper.

# Topics

* **Image labeling / object detection through deep learning** (Schober). Start with the citations of Jia, Yangqing, et al. "Caffe: Convolutional architecture for fast feature embedding." Proceedings of the ACM International Conference on Multimedia. ACM, 2014.
* **Content based image retrieval through deep learning**. Start with the citations of Wan, Ji, et al. "Deep learning for content-based image retrieval: A comprehensive study." Proceedings of the ACM International Conference on Multimedia. ACM, 2014.
* **Timed tags and deep links** (Mekul). Start with Xu, Peng, and Martha Larson. "Users Tagging Visual Moments: Timed Tags in Social Video." Proceedings of the 2014 International ACM Workshop on Crowdsourcing for Multimedia. ACM, 2014. and Vliegendhart, Raynor, et al. "How do we deep-link?: leveraging user-contributed time-links for non-linear video access." Proceedings of the 21st ACM international conference on Multimedia. ACM, 2013.
* **Late and early fusion in image and video retrieval** (Trattnig). Start with Snoek, Cees GM, Marcel Worring, and Arnold WM Smeulders. "Early versus late fusion in semantic video analysis." Proceedings of the 13th annual ACM international conference on Multimedia. ACM, 2005, then [related papers](https://scholar.google.at/scholar?q=related:kjY3DyP641QJ:scholar.google.com)
* **Image Saliency / salient objects in images** (Hamzic). Start with the citations of Toet, Alexander. "Computational versus psychophysical bottom-up image saliency: A comparative evaluation study." Pattern Analysis and Machine Intelligence, IEEE Transactions on 33.11 (2011): 2131-2146.
* **Video summaries: current state of the art** (Wakonig). Start with the citations of Potapov, Danila, et al. "Category-specific video summarization." Computer Vision–ECCV 2014. Springer International Publishing, 2014. 540-555.
* **State of the Art in Image Haze Removal** (Buchbauer). Start with He, Kaiming, Jian Sun, and Xiaoou Tang. "Single image haze removal using dark channel prior." Pattern Analysis and Machine Intelligence, IEEE Transactions on 33.12 (2011): 2341-2353., then [related papers](https://scholar.google.com/scholar?q=related:HVnj56GdVcgJ:scholar.google.com)
* **Query expansion in object retrieval**. Start with Chum, Ondrej, et al. "Total recall II: Query expansion revisited." Computer Vision and Pattern Recognition (CVPR), 2011 IEEE Conference on. IEEE, 2011., then [related papers](https://scholar.google.com/scholar?q=related:z_YhgJZWjUwJ:scholar.google.com/)
* **Game Traffic Analysis & Game Network Architecture** (Herkt). Start with Chen, Kuan-Ta, et al. "Game traffic analysis: An MMORPG perspective." Proceedings of the international workshop on Network and operating systems support for digital audio and video. ACM, 2005., then [related papers](https://scholar.google.at/scholar?q=related:Vs353cmFk6oJ:scholar.google.com/&hl=en&as_sdt=0,5) and [citations](https://scholar.google.at/scholar?cites=12291314910083272022&as_sdt=2005&sciodt=0,5&hl=en)
* **QoE in Cloud Gaming** (Lidl). Start with a [Google Scholar Search](https://scholar.google.at/scholar?q=qoe+cloud+gaming&btnG=&hl=en&as_sdt=0%2C5)
* **Interactive Omnidirectional Video** (Graf, self-proposed). Start with Kasahara, Shunichi, Shohei Nagai, and Jun Rekimoto. "First person omnidirectional video: System design and implications for immersive experience." Proceedings of the ACM International Conference on Interactive Experiences for TV and Online Video. ACM, 2015. and Rondao Alface, Patrice, Jean‐François Macq, and Nico Verzijp. "Interactive Omnidirectional Video Delivery: A Bandwidth‐Effective Approach." Bell Labs Technical Journal 16.4 (2012): 135-147. 
* **WebRTC** (Krumpholz, self-proposed). start with [Citations](https://scholar.google.at/scholar?cites=14877156367128157166&as_sdt=2005&sciodt=0,5&hl=en) 
