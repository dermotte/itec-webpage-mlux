<!--- ------------------- MAIN ------------------- -->
<blockquote><p>Human beings, who are almost unique in having the ability to learn from the experience of others, are
        also remarkable for their apparent disinclination to do so.</p>
    <footer>Douglas Adams</footer>
</blockquote> <h1>Please note!</h1>
<div class="row">
    <div class="col-md-8 panel panel-default">
        <div class="panel-body"><p class="text-justify"> This and the subsequent pages give some overview on my teaching
                activities. A thing to note before the list: Recommended reading for all students is: "<a
                        href="http://portal.acm.org/citation.cfm?id=1284635&amp;coll=Portal&amp;dl=GUIDE&amp;CFID=47044173&amp;CFTOKEN=25735463"
                        rel="external">Why you can&#039;t cite Wikipedia in my class</a>" by Neil L. Waters, which
                reflects
                my opinion very well. Also for writing any text I recommend taking a look at <a
                        href="http://www.perspektive-mittelstand.de/PR-Tipps-und-Regeln-fuer-professionelle-Zeitungsartikel/management-wissen/1636.html">how
                    newspaper articles are written (german)</a>. Furthermore, there are some <a href="?id=modalities">modalities</a>
                all courses have in common. Also note the list of <a href="?id=usefulsoftware">common and useful
                    software for students</a>. If you have questions on courses, open master or bachelor thesis project,
                send me a mail. For communication please respect the office hours and the <a
                        href="https://tools.ietf.org/html/rfc1855">netiquette</a>.</p>

            <p> If you are interested in a master's thesis, I have compiled a 
                <a href="https://docs.google.com/presentation/d/1ynYMyuXsB22T8Eb9ir9za02t-c21FkpmR1A5dOOdAAs/edit?usp=sharing">presentation</a> 
                and a detailed <a href="mdwiki">wiki</a> on how to approach that topic.</p>
            <p> Finally, there are quite some questions that come up every year, so I have a list of <a
                        href="?id=courses/faq.md">frequently asked questions (FAQ)</a> and their answers. </p></div>
    </div>
    <div class="col-md-4">
        <div class="row text-center"><img src="images/carry.png" style="margin: 2px; max-height: 180px; width: auto">
        </div>
    </div>
</div>

<h2>Summer Semester 2025</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss25.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss25.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="?id=courses/topics-game-engineering-ss25.md">Advanced Topics in Game Engineering</a> -
        Course on additional topics in game engineering with focus on creating games in a team from the Game Studies and Engineering master program.
    </li>
    <!---li>
        <a href="?id=courses/seminardms-ss23.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in interactive multimedia systems.
    </li-->
    <!--li>
        <a href="https://campus.aau.at/studium/course/116879">Practical Game Engineering</a> -
        Course on computer game engineering from the Game Studies and Engineering master program.
    </li-->
    <li>
        <a href="https://campus.aau.at/studium/course/119448">Modeling Sustainability in Board Games</a> -
        Seminar on developing board games implementing and communicating sustainability models.
    </li>
    <!--li>
        <a href="index.php?id=courses/seminardms-ss20.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in multimedia systems.
    </li-->
</ul>

<h2>Winter Semester 2024/2025</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws2425.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master program.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/topics-cg-ws24.md">VC Advanced Topics in Computer Graphics</a> -
            Course on additional topics in computer graphics with focus on computer games from the Game Studies and Engineering master program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ws24.md">VC Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <div class="li"><a href="https://campus.aau.at/studium/course/117841">UE Introduction to Computer Science</a> -
            Practical course on basic topics in computer science following and practicing the concepts of the lecture.
        </div>
    </li>
</ul>

<h2>Summer Semester 2024</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss24.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss24.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="?id=courses/topics-game-engineering-ss24.md">Advanced Topics in Game Engineering</a> -
        Course on additional topics in game engineering with focus on creating games in a team from the Game Studies and Engineering master program.
    </li>
    <!---li>
        <a href="?id=courses/seminardms-ss23.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in interactive multimedia systems.
    </li-->
    <li>
        <a href="https://campus.aau.at/studium/course/116879">Practical Game Engineering</a> -
        Course on computer game engineering from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="https://campus.aau.at/studium/course/116680">Modeling Sustainability in Board Games</a> -
        Seminar on developing board games implementing and communicating sustainability models.
    </li>
    <!--li>
        <a href="index.php?id=courses/seminardms-ss20.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in multimedia systems.
    </li-->
</ul>


<h2>Winter Semester 2023/2024</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws2324.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/topics-cg-ws23.md">VC Advanced Topics in Computer Graphics</a> -
            Course on additional topics in computer graphics with focus on computer games from the Game Studies and Engineering master program.
        </div>
    </li>
    <li>
        <div class="li"><a href="https://campus.aau.at/studium/course/117004">UE Introduction to Computer Science</a> -
            Practical course on basic topics in computer science following and practicing the concepts of the lecture.
        </div>
    </li>
</ul>

<h2>Summer Semester 2023</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss23.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss23.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="?id=courses/topics-game-engineering-ss23.md">Advanced Topics in Game Engineering</a> -
        Course on additional topics in game engineering with focus on creating games in a team from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="?id=courses/seminardms-ss23.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in interactive multimedia systems.
    </li>
    <li>
        <a href="https://campus.aau.at/studium/course/112944">Practical Game Engineering</a> -
        Course on computer game engineering from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="https://campus.aau.at/studium/course/110714">Modeling Sustainability in Board Games</a> -
        Seminar on developing board games implementing and communicating sustainability models.
    </li>
    <!--li>
        <a href="index.php?id=courses/seminardms-ss20.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in multimedia systems.
    </li-->
</ul>

<h2>Winter Semester 2022/2023</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws2223.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/topics-cg-ws22.md">VC Advanced Topics in Computer Graphics</a> -
            Course on additional topics in computer graphics with focus on computer games from the Game Studies and Engineering master program.
        </div>
    </li>
</ul>
<h2>Summer Semester 2022</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss22.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss22.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="?id=courses/topics-cg-ss22.md">Advanced Topics in Computer Graphics</a> -
        Course on additional topics in computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="#">Practical Game Engineering</a> -
        Course oncomputer game engineering from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="#">Modeling Sustainability in Board Games. Einführung Nachhaltige Entwicklung Teil III</a> -
        Seminar on developing board games implementing and communicating sustainability models.
    </li>
    <li>
        <a href="#">Peer Seminar</a> -
        A doctoral seminar focusing on fostering the abilities to rapid prototyping and self organisation.
    </li>
    <!--li>
        <a href="index.php?id=courses/seminardms-ss20.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in multimedia systems.
    </li-->
</ul>
<h2>Winter Semester 2021/2022</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws2122.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/seriousgames-ws2122.md">SE Non Entertainment Games</a> - Lecture on computer
            games that follow intentions other than entertainment, including e.g. serious games.
        </div>
    </li>
</ul>
<h2>Summer Semester 2021</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss21.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss21.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="https://campus.aau.at/studium/course/101607">Practical Game Engineering</a> -
        Course on computer game engineering from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="https://campus.aau.at/studium/course/101549">Modeling Sustainability in Board Games. Einführung Nachhaltige Entwicklung Teil III</a> -
        Seminar on developing board games implementing and communicating sustainability models.
    </li>
    <li>
        <a href="https://campus.aau.at/studium/course/101637">Peer Seminar</a> -
        A doctoral seminar focusing on fostering the abilities to rapid prototyping and self organisation.
    </li>
    <!--li>
        <a href="index.php?id=courses/seminardms-ss20.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in multimedia systems.
    </li-->
</ul>
<h2>Winter Semester 2020/2021</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws2021.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/seriousgames-ws2021.md">SE Non Entertainment Games</a> - Lecture on computer
            games that follow intentions other than entertainment, including e.g. serious games.
        </div>
    </li>
    <li>
        <div class="li"><a href="https://campus.aau.at/studium/course/103425">Cross project review</a> -
            A course for students to present and discuss their internships.
        </div>
    </li>

</ul>
<h2>Summer Semester 2020</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss20.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss20.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
    <li>
        <a href="index.php?id=courses/seminardms-ss20.md">Seminar in Multimedia Systems</a> -
        Seminar on advanced topics in multimedia systems.
    </li>
</ul>
<h2>Winter Semester 2019/2020</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws1920.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/seriousgames-ws1920.md">SE Non Entertainment Games</a> - Lecture on computer
            games that follow intentions other than entertainment, including e.g. serious games.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/sem1920.md">Seminar aus Angewandter Informatik</a> -
            Seminar as part of the bachelor curriculum round-up.
        </div>
    </li>

</ul>
<h2>Summer Semester 2019</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss19.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss19.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>
</ul>

<h2>Winter Semester 2018/2019</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/esop18">VO Einf&uuml;hrung in die
                strukturierte und objektbasierte Programmierung</a> - Basic course on object oriented programming
            focusing on Java as example.
        </div>
    </li>

    <!--li>
        <div class="li"><a href="https://campus.aau.at/studium/course/90420">PR Einf&uuml;hrung
                in die Informatik</a> - Practical course for the introductory computer science lecture.
        </div>
    </li-->
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws1819.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>

    <li>
        <div class="li"><a href="?id=courses/seriousgames-ws1819.md">SE Non Entertainment Games</a> - Lecture on computer
            games that follow intentions other than entertainment, including e.g. serious games.
        </div>
    </li>

</ul>

<h2>Summer Semester 2018</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss18.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer games planning and development from the computer science bachelor program.
        </div>
    </li>
    <li>
        <a href="?id=courses/intro-cg-ss18.md">Introduction to Computer Graphics</a> -
        Course on computer graphics with focus on computer games from the Game Studies and Engineering master program.
    </li>

    <li>
        <a href="https://campus.aau.at/studium/course/92714">Experiment/Study Planning and Data Analysis</a> -
        Course on practical data analysis and experiment design from the computer science PhD program.
    </li>

    <li>
        <a href="https://campus.aau.at/studium/course/92712">Peer Seminar</a> -
        Interactive and interdisciplinary seminar on joint bottom up research with focus on hands on and experience from the computer science PhD program.
    </li>

    <li>
        <div class="li">
            <a href="https://campus.aau.at/studium/course/90649">
                PR Einf&uuml;hrung in die Informatik
            </a> - Practical course for the introductory computer science lecture from the computer science bachelor program.
        </div>
    </li>
</ul>
<h2>Winter Semester 2017/2018</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/esop17">VO Einf&uuml;hrung in die
                strukturierte und objektbasierte Programmierung</a> - Basic course on object oriented programming
            focusing on Java as example.
        </div>
    </li>

    <li>
        <div class="li"><a href="https://campus.aau.at/studium/course/90420">PR Einf&uuml;hrung
                in die Informatik</a> - Practical course for the introductory computer science lecture.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/gameengineering-ws1718.md">VO Game Engineering</a> - Lecture on computer
            game production and development as part of the Game Studies and Engineering master programme.
        </div>
    </li>

</ul>

<h2>Summer Semester 2017</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss17.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer
            games planning and development.
        </div>
    </li>
    <li>
        <a href="?id=courses/mmisss17">Multimedia Information Systems</a> - Course on multimedia information systems and
        visual information retrieval.
    </li>

    <li>
        <div class="li">
            <a href="?id=courses/seminardms-ss17.md">
                Seminar in Distributed Systems
            </a> - Seminar in the master studies on computer science focusing on distributed multimedia systems.
        </div>
    </li>
</ul>
<h2>Winter Semester 2016/2017</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/esop16">VO Einf&uuml;hrung in die
                strukturierte und objektbasierte Programmierung</a> - Basic course on object oriented programming
            focusing on Java as example.
        </div>
    </li>

    <li>
        <div class="li"><a href="?id=courses/einfuehrungindieinformatikws1617">PR Einf&uuml;hrung
                in die Informatik</a> - Practical course for the introductory computer science lecture.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/games-ws1617.md">Introduction to Computer Game Production</a> - ERASMUS
            lecture in Toulouse on computer
            games production and development.
        </div>
    </li>

</ul>
<h2>Summer Semester 2016</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/games-ss16.md">Interaktive Multimedia-Anwendungen B (Computer Games)</a> -
            Course on computer
            games planning and development.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/einfuehrungindieinformatikss16">PR Einf&uuml;hrung
                in die Informatik</a> - Practical course for the introductory computer science lecture.
        </div>
    </li>
    <li>
        <div class="li">
            <a href="?id=courses/seminardms-ss16.md">
                Seminar in Distributed Systems
            </a> - Seminar in the master studies on computer science focusing on distributed multimedia systems.
        </div>
    </li>
</ul>
<h2>Winter Semester 2015/2016</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/esop15">VO Einf&uuml;hrung in die
                strukturierte und objektbasierte Programmierung</a> - Basic course on object oriented programming
            focusing on Java as example.
        </div>
    </li>

    <li>
        <div class="li"><a href="?id=courses/einfuehrungindieinformatikws1516">PR Einf&uuml;hrung
                in die Informatik</a> - Practical course for the introductory computer science lecture.
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/mediainformatik2015">Introduction to
                Media Informatics</a> - Introduction to media technologies course.
        </div>
    </li>
</ul>
<h2>Summer Semester 2015</h2>
<ul>
    <li><a href="?id=courses/ringvorlesungss15">Ringvorlesung Informatik und Informationstechnik</a> - Lecture with
        rotating speakers for PhD students
    </li>
    <li><a href="?id=courses/gamesss15">Interaktive Multimedia-Anwendungen B (Computer Games)</a> - Course on computer
        games planning and development.
    </li>
    <li><a href="?id=courses/mmisss15">Multimedia Information Systems</a> - Course on multimedia information systems.
    </li>
</ul>
<h2>Winter Semester 2014/2015</h2>
<ul>
    <li>
        <div class="li"><a href="?id=courses/sem14" title="courses:sem11">Seminar aus Angewandte Informatik</a> -
            Seminar as part of the bachelor course round-up
        </div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/esop14" class="wikilink2" title="courses:esop14">VO Einf&uuml;hrung in die
                strukturierte und objektbasierte Programmierung</a> - Basic course on object oriented programming
            focusing on Java as example.
        </div>
    </li>
    <li>
        <div class="li"><a href="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=80393"
                           rel="external"
                           title="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=80393">PR Einf&uuml;hrung
                in die Informatik</a></div>
    </li>
    <li>
        <div class="li"><a href="?id=courses/mediainformatik2014" title="courses:mediainformatik2014">Introduction to
                Media Informatics</a></div>
    </li>
</ul> <h2>Summer Semester 2014</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss14" title="courses:mmisss14"> VK Multimedia
                    Information Systems</a> - Course on multimedia information systems.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamesss14" title="courses:gamesss14"> VK Computer
                    Games</a> - Course on computer games planning and development.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmtech14" title="courses:mmtech14"> Introduction to
                    Multimedia Technology (DMS)</a> - Basic course on multimedia technologies
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/posterpresentation2014"
                               title="courses:posterpresentation2014"> Scientific Poster Presentation</a> - Half day
                course on scientific poster presentation.
            </div>
        </li>
    </ul>
</div> <!-- EDIT3 SECTION "Summer Semester 2014" [1300-1777] --> <h2>Winter Semester 2013 / 2014</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mediainformatik2014"
                               title="courses:mediainformatik2014"> Introduction to Media Informatics and Media
                    Informatics Lab</a> - Introductory course on media informatics.
            </div>
        </li>
    </ul>
</div> <!-- EDIT4 SECTION "Winter Semester 2013 / 2014" [1778-1962] --> <h2>Summer Semester 2013</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss13" title="courses:mmisss13"> VK Multimedia
                    Information Systems</a> - Course on multimedia information systems.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamesss13" title="courses:gamesss13"> VK Computer
                    Games</a> - Course on computer games planning and development.
            </div>
        </li>
    </ul>
</div> <!-- EDIT5 SECTION "Summer Semester 2013" [1963-2201] --> <h2>Winter Semester 2012 / 2013</h2>
<div>
    <ul>
        <li>
            <div class="li"><a href="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=74614"
                               rel="external"
                               title="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=74614">Einf&uuml;hrung
                    in die Informatik </a> - Practical part of an introductory course for new CS students.
            </div>
        </li>
    </ul>
</div> <!-- EDIT6 SECTION "Winter Semester 2012 / 2013" [2202-2416] --> <h2>Summer Semester 2012</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss12" title="courses:mmisss12"> VK Multimedia
                    Information Systems</a> - Course on multimedia information systems.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamesss12" title="courses:gamesss12"> VK Computer
                    Games</a> - Course on computer games planning and development.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/semss12" title="courses:semss12">Seminar aus
                    Betriebsinformatik f&uuml;r Informationsmanagement</a></div>
        </li>
    </ul>
</div> <!-- EDIT7 SECTION "Summer Semester 2012" [2417-2739] --> <h2>Winter Semester 2011 / 2012</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/prosem11" title="courses:prosem11"> Proseminar
                    Angewandte Informatik</a> - Proseminar
            </div>
        </li>
        <li>
            <div class="li"><a href="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=69474"
                               rel="external"
                               title="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=69474">Einf&uuml;hrung
                    in die Informatik </a></div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamespassau11" title="courses:gamespassau11"> Games
                    course in Passau, DE</a> - Lecture
            </div>
        </li>
    </ul>
</div> <!-- EDIT8 SECTION "Winter Semester 2011 / 2012" [2740-3031] --> <h2>Summer Semester 2011</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss11" title="courses:mmisss11"> VK Multimedia
                    Information Systems</a> - Course on multimedia information systems.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamesss11" title="courses:gamesss11"> VK Computer
                    Games</a> - Course on computer games planning and development.
            </div>
        </li>
    </ul>
</div> <!-- EDIT9 SECTION "Summer Semester 2011" [3032-3271] --> <h2>Winter Semester 2010 / 2011</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/prosem10" title="courses:prosem10"> Proseminar
                    Angewandte Informatik</a> - Proseminar
            </div>
        </li>
        <li>
            <div class="li"><a href="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=66991"
                               rel="external"
                               title="https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=66991">Einf&uuml;hrung
                    in die Informatik </a></div>
        </li>
    </ul>
</div> <!-- EDIT10 SECTION "Winter Semester 2010 / 2011" [3272-3495] --> <h2>Summer Semester 2010</h2>
<div>
    <ul>
        <li>
            <div class="li"><a href="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=63651"
                               rel="external"
                               title="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=63651">PR
                    Compiler Construction</a> - Practical course on compilers.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss10" title="courses:mmisss10"> VK Multimediale
                    Informationssysteme</a> - Course on multimedia information systems.
            </div>
        </li>
    </ul>
</div> <!-- EDIT11 SECTION "Summer Semester 2010" [3496-3779] --> <h2>Winter Semester 2009 / 2010</h2>
<div>
    <ul>
        <li>
            <div class="li"><a href="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=63469"
                               rel="external"
                               title="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=63469">PR
                    Einf&uuml;hrung in die Informatik</a> - Practical part of an introductory course to computer
                science.
            </div>
        </li>
        <li>
            <div class="li"><a href="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=62263"
                               rel="external"
                               title="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=62263">
                    PR Rechnernetze</a> - Practical part of a course on computer networks
            </div>
        </li>
        <li>
            <div class="li"><a href="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=64138"
                               rel="external"
                               title="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=64138">
                    Games Lab</a> - Our new lab course to create games: students create games and participate at the
                Imagine Cup.
            </div>
        </li>
    </ul>
</div> <!-- EDIT12 SECTION "Winter Semester 2009 / 2010" [3780-4347] --> <h2>Summer Semester 2009</h2>
<div>
    <ul>
        <li>
            <div class="li"><a href="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=60609"
                               rel="external"
                               title="https://campus.uni-klu.ac.at/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=60609">PR
                    Compiler Construction</a> - Practical course on compilers.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamesss09" title="courses:gamesss09"> VK Computer
                    Games</a> - Course on basics for computer game development focusing on 2D arcade games.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss09" title="courses:mmisss09"> VK Multimediale
                    Informationssysteme</a> - Course on multimedia information systems.
            </div>
        </li>
    </ul>
</div> <!-- EDIT13 SECTION "Summer Semester 2009" [4348-4755] --> <h2>Winter Semester 2008 / 2009</h2>
<div>
    <ul>
        <li>
            <div class="li"><a
                        href="https://wwws.uni-klu.ac.at/uniklu/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=60380"
                        rel="external"
                        title="https://wwws.uni-klu.ac.at/uniklu/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=60380">PR
                    Einf&uuml;hrung
                    in die Informatik</a> - Practical part of an introductory course to computer science.
            </div>
        </li>
        <li>
            <div class="li"><a
                        href="https://wwws.uni-klu.ac.at/uniklu/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=60526"
                        rel="external"
                        title="https://wwws.uni-klu.ac.at/uniklu/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=60526"> PR
                    Rechnernetze</a> - Practical part of a course on computer networks
            </div>
        </li>
    </ul>
</div> <!-- EDIT14 SECTION "Winter Semester 2008 / 2009" [4756-5143] --> <h2>Summer Semester 2008</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss08" title="courses:mmisss08"> VK Multimediale
                    Informationssysteme</a> - Course on multimedia information systems.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/gamesss08" title="courses:gamesss08"> VK Computer
                    Games</a> - Course on computer games development.
            </div>
        </li>
        <li>
            <div class="li"><a
                        href="https://wwws.uni-klu.ac.at/uniklu/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=57089"
                        rel="external"
                        title="https://wwws.uni-klu.ac.at/uniklu/studien/lvkarte.jsp?sprache_nr=35&amp;rlvkey=57089">PR
                    Compiler Construction</a> - Practical course on compiler construction
            </div>
        </li>
    </ul>
</div> <!-- EDIT15 SECTION "Summer Semester 2008" [5144-5530] --> <h2>Winter Semester 2007 / 2008</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/einfuehrungindieinformatikws0708"
                               title="courses:einfuehrungindieinformatikws0708"> PR Einf&uuml;hrung in die
                    Informatik</a> - Practical part of an introductory course to computer science.
            </div>
        </li>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/rechnernetzews0708"
                               title="courses:rechnernetzews0708"> PR Rechnernetze</a> - Practical part of a course on
                computer networks
            </div>
        </li>
    </ul>
</div> <!-- EDIT16 SECTION "Winter Semester 2007 / 2008" [5531-5826] --> <h2>Summer Semester 2007</h2>
<div>
    <ul>
        <li>
            <div class="li"><a data-ajax="false" href="?id=courses/mmisss07" title="courses:mmisss07"> VK Multimediale
                    Informationssysteme</a> - Course on multimedia information systems.
            </div>
        </li>
        <li>
            <div class="li"> RE Rechnernetze - Revision course on computer networks</div>
        </li>
    </ul>
</div> <!-- EDIT17 SECTION "Summer Semester 2007" [5827-6028] --> <h2>Winter Semester 2006 / 2007</h2>
<div>
    <ul>
        <li>
            <div class="li"> PR Rechnernetze - Practical part of a course on computer networks</div>
        </li>
        <li>
            <div class="li"> PR Distributed Multimedia Systems - Practical part of a course on multimedia coding and
                networks
            </div>
        </li>
    </ul>
</div> <!-- EDIT18 SECTION "Winter Semester 2006 / 2007" [6029-6242] --> <h2>Summer Semester 2006</h2>
<div>
    <ul>
        <li>
            <div class="li"> VU Wissenstechnologien - A course on semantic web, metadata and ontologies, TU Graz</div>
        </li>
    </ul>
</div> <!-- EDIT19 SECTION "Summer Semester 2006" [6243-6366] --> <h2>Winter Semester 2006 / 2007</h2>
<div>
    <ul>
        <li>
            <div class="li"> Einf&uuml;hrung in das wissenschaftliche Arbeiten - An introductory seminar to scientific
                work and writing, TU Graz
            </div>
        </li>
    </ul>
</div>
