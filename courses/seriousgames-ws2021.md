# Non Entertainment Games

The goal of this course is to introduce students to games that are mainly created for purposes other than entertainment. This includes but is not limited to serious games, educational games, adware games, fitness games, and propaganda games.

## Online, but How?

Due to the restrictions necessary to prevent the spread of COVID-19 we are going to teach all courses in Game Studies and Engineering [online](https://www.twitch.tv/dermotte_). Please make sure to join the GSE Discord Server to stay up to date [https://discord.gg/YnX6AKG](https://discord.gg/YnX6AKG)


## Schedule

All courses start s.t. if not otherwise noted.

| Day    | Time   | Place  | Topic | Links |
|:------|:------|:------|:---|:---|
| Th, 15.10. | 10:00-11:30 | online | Introduction | [slides](https://docs.google.com/presentation/d/17KdfCADBHfC4_lqpZfSAWdtWzgirU9xWzvafeiufI38/edit?usp=sharing), [watch video](https://youtu.be/QrGQtd2qYA8), [download video](https://www2.itec.aau.at/owncloud/index.php/s/OXKxozB9VJz9RSI) |
| Th, 22.10. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | GWAPs & topic distribution | [slides](https://docs.google.com/presentation/d/1nfFCDIwJYXcFU5wKaYow937aSDQKq2ZH5VbLcYRnqAA/edit?usp=sharing), [watch video](https://youtu.be/uhNXy3nsJ_Y), [download video](https://www2.itec.aau.at/owncloud/index.php/s/LWrKANCQt9ZxLYW) |
| Th, 29.10. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | Flow Assessment for Games | [slides](https://docs.google.com/presentation/d/1_6a_CC1-Hc-iWfXDmYpDJQDy7QnC-SUx8vnzb7XpD3w/edit?usp=sharing), [watch video](https://youtu.be/s0WIsQVke0E), [download video](https://www2.itec.aau.at/owncloud/index.php/s/9QwcFvAd7OO8vIo) |
| Th, 5.11. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | Serious Games | [slides](https://docs.google.com/presentation/d/1np3XvvTKldU4PXYJ-q3cczZoh0atOFuPpaA62gFt3a8/edit?usp=sharing) |
| Th, 12.11. | 10:00-11:30 | Discord | Intermediate presentations | none |
| Th, 10.12. | 10:00-11:30 | Discord | Q&A (optional: drop by if you have questions) | none |
| Th, 14.01. | 10:00-11:30 | Discord | Q&A (optional: drop by if you have questions) | none |
| Th, 21.01. | 10:00-14:00 | Discord | Student Presentations (lunch break 11:30-12:30) | schedule in Discord |
| Th, 28.01. | 16:00-19:00 | Discord | Student Presentations |  schedule in Discord |




## Modalities

This course is a _Seminar_, meaning that the grade is based on active participation in the seminar as well as a final seminar presentation & written work. There will be a blocked lecture on Jan 21 & Jan 28, where the group plays and discusses a game of that type.

A positive grade can be achieved by reaching at least Level 3 in the [Experience System](https://docs.google.com/spreadsheets/d/1bVMx1yMFUEj0xx668i2HjwBJt9RncX_cveRDOdVdxH8/edit?usp=sharing) for the class. **Mandatory tasks** are

* Giving the intermediate presentation on Nov 12, 2020. [sample presentation](https://docs.google.com/presentation/d/1O-yT9JJSjKepgk9_VMDj44UPz9BoOCXYP1CI6w4Rf1c/edit?usp=sharing) (3 minutes presentation and 2 minutes of Q&A)
* Giving a presentation at the end of Jan 2021: 10 minutes of presentation and 5 minutes of discussion each on Discord.
* Handing in the seminar report (1500 words) until Feb 28, 2021.  

## Topics

[Topics and distribution thereof](https://docs.google.com/spreadsheets/d/1Qd5cXUTAGjZHup9iEyaIsyCfjcRQjAV04ZHJeaqCrf0/edit?usp=sharing)
