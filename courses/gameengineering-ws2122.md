# Game Engineering

The goal of this course is to introduce students to computer game engineering. This is
not necessarily a technical issue, especially as game engineering involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing.

After this course, students should be able to understand the concepts and roles within a game project with the
knowledge of how a game project can be set up and run, as well as detailed knowledge on particular processes within the
project.

The practical course associated with the lecture is taking place as a game jam. Find more information on the [game jam website](http://itec.aau.at/gamejam/) soon.

## Online, but How?

Due to the restrictions necessary to prevent the spread of COVID-19 we are going to teach all courses in Game Studies and Engineering online. Please make sure to join the GSE Discord Server to stay up to date [https://discord.gg/YnX6AKG](https://discord.gg/YnX6AKG)

The game engineering lectures take place on [twitch.tv](https://www.twitch.tv/dermotte_) and are available as recordings afterwards, either on Twitch, on [YouTube](https://www.youtube.com/playlist?list=PLkb7TymgoWW6L3mTsqvqhzavvAw33bfi6), or [for download](https://www2.itec.aau.at/owncloud/index.php/s/VAQBKp1SAOjM0dP)

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/1uLf-8FBfsgHX_tDekdElde5RjsmQ6RJfm7T-q3fboAg/edit?usp=sharing)
2. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/1KqYfMRRYD6DjZCxdzc0o-rxw6Rq61yQ8G4NS3hSjwXo/edit?usp=sharing)
3. Game Projects: Documents and Pitches ~ slides see above
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/1KeEcCWo4rs4BUlQd49zR3WNFY_AV4lsOxPKxfWtsfSA/edit?usp=sharing)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1VBVcga4Apfwzg2qLGGDkKFEVIlhZJH18-eE6tUixevk/edit?usp=sharing)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1f_hveciVgx6O5afQYen-fJEjk7_rn7a0eF3sRR8Ldgo/edit?usp=sharing)
7. Playtests ~ [slides](https://docs.google.com/presentation/d/1CfsKY73k-GqNlu6xhgajfLQ1bJ1qEzX7ZEhTfvwyCZk/edit?usp=sharing)
8. Animation ~ [slides](https://docs.google.com/presentation/d/1wE2eoxbFd-ipazu9PH4xrX-UldaNanvOhVZ90iMu1GI/edit?usp=sharing)
9. Sound & Interactive Music ~ [slides](https://docs.google.com/presentation/d/1VSJLcvutrB9dF0HiaYmbCxVg8HqefDuKih-2MOLuHso/edit?usp=sharing)
10. Virtual Reality and the Uncanny Valley ~ [slides](https://docs.google.com/presentation/d/1V9C14ur8FoAuLwNMe2ZpZGr9sTrTPDhurgydQPxguHM/edit?usp=sharing)
11. Tools & Tool Chain ~ [slides](https://docs.google.com/presentation/d/1MwfZDZWA1bp4elYJCAOx4YNeB1H__eVZ7iPt9F-5PEI/edit?usp=sharing)

## Modalities

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The open book exam will be online, take 60 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://docs.google.com/document/d/1Z3XhU1tYw1JmpIryDF9UBoEK8fPYe5nruExHBayOzMU/edit?usp=sharing).

The exam will take place in Moodle on Jan 24 at 10 am CET (just like the lecture). It will take 30 minutes of your time. Please make sure you are registered to the exam and can access Moodle.

## Links
Here are some links to dive further into the topics of the course.

### Programming
Getting startetd with programming without prior knowledge ..
- [Hour of Code](https://code.org) - Learn to program with a visual programming lecture
- [Scratch](https://mit.scratch.edu) - Use the same visual programming lecture to create interactive programs and games

### Prototyping
Simple engines for sketching out ideas.
- [Twine](https://twinery.org/) - An open-source tool for telling interactive, nonlinear stories.
- [Ren'Py](https://www.renpy.org/) - A visual novel engine

### Informative and fun videos
You want to know more? Here's a lot of material that goes beyond the course.
- [Extra Credits](https://www.youtube.com/user/ExtraCreditz) - Covers many basic aspects of game dev
- [GDC Vault](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ) - Full length presentations from GDC
