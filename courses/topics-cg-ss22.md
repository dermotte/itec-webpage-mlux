# Advanced Topics in Computer Graphics

This course will dive into specific topics in the broad area of computer graphics. While details have not yet been decided, the direction will be procedurally generated content (PCG) and AR/VR. The course will require a significant amount of practical work, so programming knowledge is required. We will implement standard algorithms for PCG in computer games and put them to use in a VR environment. The web page will be updated continuously. Grading is based on (i) mandatory assignments, (ii) a final project, and (iii) a final exam.

Find the lecture schedule at [the AAU Campus System](https://campus.aau.at/studium/course/108262).

All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below). Due to the situation around CoViD-19 we start the summer term 2022 with online lectures and see where it leads us. After the online courses, lecture recordings are available on [YouTube](https://www.youtube.com/playlist?list=PLkb7TymgoWW6aH5TL3vqFO6Yut_ZHM5tU) and for [download](https://www2.itec.aau.at/owncloud/index.php/s/kzAyrtJ8MGTfIGA). Live lectures are taking place on the Twitch Channel [https://www.twitch.tv/dermotte_](https://www.twitch.tv/dermotte_).

## Topics

* Introduction to procedurally generated content ~ [slides](https://docs.google.com/presentation/d/1kfMYhwyTY6lJpBz3EF1hDsFMvznGNO70ViIAVrurZI8/edit?usp=sharing)
* Overview on PCG algorithms and pseudo-random number generators ~ [slides](https://docs.google.com/presentation/d/1Kx3LsOAwJElVhcPT5JlRoiiYiOkNe7sMdQeFdLGCWag/edit?usp=sharing)
* Noise functions ~ [slides](https://docs.google.com/presentation/d/1NMXKDFgB8Obgl251MHgSGNbQ_lQ1BC3J9EYqejH2E2s/edit?usp=sharing)
* Labyrinths ~ [slides](https://docs.google.com/presentation/d/1y-C51GIyKOYfdTDU2dFj1FqqWb2fRZm2vE3WflkWhRM/edit?usp=sharing)
* L-Systems & Erosion ~ [slides](https://docs.google.com/presentation/d/1ZW41FT0vXz7BK508DkbOHYJbExxsAw_66U1HcS_MZJI/edit?usp=sharing)
* Introduction to VR ~ [slides](https://docs.google.com/presentation/d/1LMHQ0bHJGcYOHh5QZ5mK8HTPGn-TQIHiMINbCqZLZXs/edit?usp=sharing)
* WebXR ~ [slides](https://docs.google.com/presentation/d/1bT3IXcBocENsP1iZW2_gCwBU3p7ItUwdJhtfnQEMoq8/edit?usp=sharing)

Find the source code used in the lecture on Github: https://github.com/dermotte/AdvancedTopicsCG-ST2022

## Exercises (Mandatory Assignments)

Please upload the exercises (no .exe please, stick to .js, .pde or export to the web) to Moodle until the end of the announced day.

| ID | Title | Slide | Deadline |
|:---|:---|:---|:---|
| 01 | 3D Game of Life | [#30](https://docs.google.com/presentation/d/1kfMYhwyTY6lJpBz3EF1hDsFMvznGNO70ViIAVrurZI8/edit?usp=sharing) | March 30, 2022 |
| 02 | 2D PRNG | [#40](https://docs.google.com/presentation/d/1Kx3LsOAwJElVhcPT5JlRoiiYiOkNe7sMdQeFdLGCWag/edit?usp=sharing) | April 6, 2022 |
| 03 | Perlin Noise Shader | [#45](https://docs.google.com/presentation/d/1NMXKDFgB8Obgl251MHgSGNbQ_lQ1BC3J9EYqejH2E2s/edit?usp=sharing) | April 20, 2022 |
| 04 | 3D Maze | [#55](https://docs.google.com/presentation/d/1y-C51GIyKOYfdTDU2dFj1FqqWb2fRZm2vE3WflkWhRM/edit?usp=sharing) | May 11, 2022 |

## Final Project

Present your final project on June 30, 2022. We've reserved the Game Lab, but you can join us online too. For the presentation (max duration 5 minutes):

1. show your result,
2. outline your work (contribution), and
3. discuss the lessons learned and obstacles encountered.

For the submission of your final project:

1. Your work as source code or a link to Github, Bitbucket, etc.
2. Your result as rendering (images, movie)
3. Your slides from the presentation
4. A PDF or TXT including your name and student number and eventual relevant links (Github, YouTube, etc.)

## Exam

The exam will take place on June 30, at the beginning of the lecture. It will be a Moodle quiz for those joining online and a written paper exam for those in the game lab. A list of possible [Exam Questions](https://docs.google.com/document/d/1PNGdTs1q9d2VlXX6aV0t806EMxatkvFPCpGf9ylwJpQ/edit?usp=sharing) is available.

## Links

* [p5js](https://p5js.org/) - JavaScript framework used in the course for examples
* [Processing](https://processing.org/) - Java based implementation of above framework
* [Löve](https://love2d.org/) - Lua based game programming framework used in the course for examples
