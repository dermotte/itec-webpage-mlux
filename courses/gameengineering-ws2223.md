# Game Engineering

The goal of this course is to introduce students to computer game engineering. This is
not necessarily a technical issue, especially as game engineering involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing.

After this course, students should be able to understand the concepts and roles within a game project with the
knowledge of how a game project can be set up and run, as well as detailed knowledge on particular processes within the
project.

The practical course associated with the lecture is taking place as a game jam. Find more information on the [game jam website](http://itec.aau.at/gamejam/) soon.

## Online, but How?

Due to the international nature of the Game Studies and Engineering program, we will start the semester with a hybrid lecture. Please find the virtual classroom in [Moodle](https://moodle.aau.at/course/view.php?id=35650). Guest access in Moodle is enabled for the beginning of the semester. There, we will broadcast from the lecture room. Note that we only support streaming of audio and the slides right now.

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/1ZRjapdHn_Pk5UAM5yigxdJIZfIW2ifKRnQvLnPysGxk/edit?usp=sharing)
2. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/1uctkZjNBrEnxUpXgnHE6Xe3jiWrHp83C-HIWB3MkgM0/edit?usp=sharing)
3. Game Projects: Documents and Pitches ~ slides see above, [recorded lecture](https://youtu.be/QKBxPujzE70)
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/1vSkjkyN8cltdXx3XluELF_pLrhRZ5KwG2YvnZef8Zuo/edit?usp=sharing)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1RMVDQ4RN0r8ADPYy9P7xtcZx49tBDjwZhZtgrS4Pp3I/edit?usp=sharing)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1v_IHaPsgqzyD2cvwWWUYRPlEm0IZhl7X0y3uisiTN8Y/edit?usp=sharing)
7. Playtests ~ [slides](https://docs.google.com/presentation/d/12oZecCtryxH8J5lKcMrwVOGkmVNVUbfEx-KSRvEDa_4/edit?usp=sharing)
8. Animation ~ [slides](https://docs.google.com/presentation/d/1E6w8bCO9S9Ht1bkMXD4r46GyHLCo_YES2qgbq5Vcmsk/edit?usp=sharing)
9. Sound & Interactive Music ~ [slides](https://docs.google.com/presentation/d/1ZHQWWW8UMzOCcbpryEaFh-yOzb9wahDB-vV_Loh-lgg/edit?usp=sharing)
10. Tools & Tool Chain ~ [slides](https://docs.google.com/presentation/d/1kVEIdmztM8euz1cL5n3Xtnyzz-wZGT2X3iNUGqVQ_qc/edit?usp=sharing)

## Modalities

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The open book exam will take 60 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://docs.google.com/document/d/1mvdgXJl7ckqvTvY6unqEGGiHF7bwgz4frzoh4uxip9o/edit?usp=sharing).

## Links
Here are some links to dive further into the topics of the course.

### Programming
Getting startetd with programming without prior knowledge ..
- [Hour of Code](https://code.org) - Learn to program with a visual programming lecture
- [Scratch](https://mit.scratch.edu) - Use the same visual programming lecture to create interactive programs and games

### Prototyping
Simple engines for sketching out ideas.
- [Twine](https://twinery.org/) - An open-source tool for telling interactive, nonlinear stories.
- [Ren'Py](https://www.renpy.org/) - A visual novel engine

### Informative and fun videos
You want to know more? Here's a lot of material that goes beyond the course.
- [Extra Credits](https://www.youtube.com/user/ExtraCreditz) - Covers many basic aspects of game dev
- [GDC Vault](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ) - Full length presentations from GDC
