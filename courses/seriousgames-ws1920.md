# Non Entertainment Games

The goal of this course is to introduce students to games that are mainly created for purposes other than entertainment. This includes but is not limited to serious games, educational games, adware games, fitness games, and propaganda games. 

## Schedule

All courses start s.t. if not otherwise noted.
 
| Day    | Time   | Place  | Topic | Links |
|:------|:------|:------|:---|:---|
| Mo, 07.10. | 14-16 | S.2.69 | Introduction & GWAPs | [slides](https://docs.google.com/presentation/d/1ptKeQw_VoDga8M8mMkkBPryw0ZT0yfx8kYChIb_D-PM/edit?usp=sharing) |
| Mo, 14.10. | 14-16 | S.2.69 | Introduction & Preparations for the blocked lecture | [slides](https://docs.google.com/presentation/d/1PEmKXNxdRudDt3Hc3vU24_stG12TdFK3zB0KDLRmCt0/edit?usp=sharing) |
| Mo, 04.11. | 14-16 | S.2.69 | Flow Assessment for Games | [slides](https://docs.google.com/presentation/d/1rQy-jHwp9B752s37sYGtOXiTJbJ8ltfl5ATuHsDKTIE/edit?usp=sharing) |
| Mo, 18.11. | 14-16 | S.2.69 | Intermediate presentations | [sample presentation](https://docs.google.com/presentation/d/14YS5aABSIQxwR3r6L2G8V2p0w7j9fUCY_ESygY_q7hY/edit?usp=sharing) |
| We, 11.12. | 09-15 | tba. | Lecture Block | tba. |
| Mo, 16.12. | 14-16 | S.2.69 | Reflection on the blocked lecture | tba. |
| Mo, 13.01. | 14-16 | S.2.69 | Final Presentations I | tba. |
| Mo, 20.01. | 14-16 | S.2.69 | Final Presentations II | tba. |



## Modalities 

This course is a _Seminar_, meaning that the grade is based on active participation in the seminar as well as a final seminar presentation & written work. There will be a blocked lecture on XXX, where the group plays and discusses a game of that type. 

A positive grade can be achieved by reaching at least Level 3 in the [Experience System](https://docs.google.com/spreadsheets/d/16_5z4R3SG6Kl9Ry8p2V61qULzmzyA4tEnDZ-kgHv2R8/edit?usp=sharing) for the class. **Mandatory tasks** are

* Giving the intermediate presentation.
* Giving a presentation.
* Handing in the seminar report.  

### Intermediate presentations
Use the  [sample presentation](https://docs.google.com/presentation/d/14YS5aABSIQxwR3r6L2G8V2p0w7j9fUCY_ESygY_q7hY/edit?usp=sharing) as a starting point and give the information on what you plan to do there. Note, that you only got a few minutes to talk, so don't invest too much. Moreover, I intend to employ the intermediate presentations more like a topic pitch, meaning that we can - at the time of presentation - adjust topics a little bit to avoid overlap. 

Make sure to put your presentations, name and topic in the list: [Non-Entertainment Games - Topics](https://docs.google.com/spreadsheets/d/1UNktfNzeH9_PEZPGQFJpUslQzTOM9M1yCY0a1X3wNRk/edit?usp=sharing) 

### Final presentations
Please prepare a presentation on your topic with a length of 8 minutes. Make sure to reserve a presentation slot by giving name and topic in the list: [Non-Entertainment Games - Schedule](https://docs.google.com/spreadsheets/d/1XPU8DXMx2wqOyy3Unwo2hwjLWtQk3_QwYM7S3mooNsA/edit?usp=sharing) 

## Topics

* Propaganda games
* Ad games
* Fitness games
* Educational games
* Games in health care
* Games for job training
* Experience games
* Achievements as game mechanic in non game contexts
* etc.