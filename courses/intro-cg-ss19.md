# Introduction to Computer Graphics

This course aims to give a basic understanding of computer graphics and its role in computer games. 
Topics will be a math primer, 2D and 3D graphics, shading, and hands on with practical tools. 
Grading is based on mandatory assignments as well as a final projects done as team work. 

## Schedule

| Day    | Time   | Place  | Topic | Readings |
|:------|:------|:------|:---|:---|
| Do, 07.3.  | 16-18 | S.1.42 | Introduction ~ [slides](https://docs.google.com/presentation/d/1F_0qw8_CVoq0Lr-e0c5xs797SHucuc64z-igL0_WyKA/edit?usp=sharing)|  [Sand rendering in Journey](https://www.youtube.com/watch?v=wt2yYnBRD3U) |
| Do, 14.3.  | 16-18 | S.1.42 | Basics Maths for Computer Graphics ~ [slides](https://docs.google.com/presentation/d/1epmUN0KnNnhEVA6m21NgCtrXDjsFQiMOoldInaMfTOk/edit?usp=sharing)| [Easing functions](https://www.youtube.com/watch?v=mr5xkf6zSzk) |
| Do, 21.3.  | 16-18 | S.1.42 | Coordinate Systems and Matrices ~ [slides](https://docs.google.com/presentation/d/16iKm1Qhogo12mv6j_lrNp6K7gFsjUyeBfp3Pvec7W9Y/edit?usp=sharing) | [Stable Elasticity from Pixar (just the video)](http://graphics.pixar.com/library/StableElasticity/) |
| Do, 28.3.  | 16-18 | S.1.42 | Meshes and Lights ~ [slides](https://docs.google.com/presentation/d/1Hae5vxUC7A3y6k6rsMqVrqllWvwpEQEFYUYoKZdOl-k/edit?usp=sharing) | [Three Point Lighting](https://youtu.be/Bmme1slWdm4) |
| Do, 11.4.  | 16-18 | S.1.42 | Shading and Cameras ~ [slides](https://docs.google.com/presentation/d/1nUHJ4KNMLgdRRi0dLOvtgQKRiGYXf0kWw0MGygK5REc/edit?usp=sharing) | [What are shaders?](https://www.youtube.com/watch?v=sXbdF4KjNOc) |
| Do, 09.5.  | 16-18 | S.1.42 | Shading and Cameras (ctd.) ~ [slides](https://docs.google.com/presentation/d/1nUHJ4KNMLgdRRi0dLOvtgQKRiGYXf0kWw0MGygK5REc/edit?usp=sharing) | none |
| Do, 23.5.  | 16-18 | S.1.42 | Textures ~ [slides](https://docs.google.com/presentation/d/18mXoMIH5sjfR5c0oHLUYHi9MqELIhqGVvOtRZ9TtYh0/edit?usp=sharing) | [Three.js Intro](https://youtu.be/6eLl8yQnxHQ) |
| Do, 06.6.  | 16-18 | S.1.42 | Real Time Rendering, GPUs, Raytracing ~ [slides](https://docs.google.com/presentation/d/1ZkenXx-txyGVFAJnmmYCdYias8zqI-595OUM8g6ETNU/edit?usp=sharing) | [Pixar Renderman Showreel](https://www.youtube.com/watch?v=E3vPJybuG9s) |
| Fr, 07.6.  | 08-12 | S.2.69 | Blender Fundamentals (see also [1], videos 1-11) | [Low Poly Illustrations in Blender](https://cgi.tutsplus.com/tutorials/secrets-to-creating-low-poly-illustrations-in-blender--cg-31770) |
| Mi, 12.6.  | 14-16 | S.2.69 | Blender Modeling | [Low Poly Animals Tutorial](https://www.youtube.com/watch?v=JjW6r10Mlqs) |
| Do, 27.6.  | 16-18 | S.1.42 | Presentations  | tba. |

### References and Links
 1. [Processing](https://processing.org/) - Java based software for the first part of the course
 1. [Blender Tutorials](https://www.youtube.com/playlist?list=PLa1F2ddGya_8V90Kd5eC5PeBjySbXWGK1) - Learn Blender on your wn
 1. [CC0 Textures](https://cc0textures.com/) - CC0 licensed high quality textures
 1. [Thingiverse](https://www.thingiverse.com/) - Database of printable 3D objects

## Exercises

Please send the exercises to M. Lux until the end of the announced day. 

| ID | Title | Slide | Deadline | Example Solution |
|:---|:---|:---|:---|:---|
| EX 02-01 | Vector Reflection | Slide 30 in [Lecture 02](https://docs.google.com/presentation/d/1epmUN0KnNnhEVA6m21NgCtrXDjsFQiMOoldInaMfTOk/edit?usp=sharing) | Apr 10, 2019 | ... |
| EX 02-02 | The Lerp Game | Slide 42 in [Lecture 02](https://docs.google.com/presentation/d/1epmUN0KnNnhEVA6m21NgCtrXDjsFQiMOoldInaMfTOk/edit?usp=sharing) | Apr 10, 2019 | ... |
| EX 03-01 | Two Boxes | Slide 38 in [Lecture 03](https://docs.google.com/presentation/d/16iKm1Qhogo12mv6j_lrNp6K7gFsjUyeBfp3Pvec7W9Y/edit?usp=sharing) | Apr 30, 2019 | ... |
| EX 04-01 | Lights | Slide 35 in [Lecture 04](https://docs.google.com/presentation/d/1Hae5vxUC7A3y6k6rsMqVrqllWvwpEQEFYUYoKZdOl-k/edit?usp=sharing) | Apr 30, 2019 | ... |
| EX 05-01 | Camera | Slide 41 in [Lecture 05](https://docs.google.com/presentation/d/1nUHJ4KNMLgdRRi0dLOvtgQKRiGYXf0kWw0MGygK5REc/edit?usp=sharing) | Jun 24, 2019 | ... |
| EX 06-01 | Six Sided Die | Slide 24 in [Lecture 06](https://docs.google.com/presentation/d/18mXoMIH5sjfR5c0oHLUYHi9MqELIhqGVvOtRZ9TtYh0/edit?usp=sharing) | Jun 24, 2019 | ... |



## Materials

All the materials from the course are available in the Github repo at [https://github.com/dermotte/cg-lecture-2019](https://github.com/dermotte/cg-lecture-2019).