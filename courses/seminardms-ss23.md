# Seminar Multimedia Systems (2023)
In this seminar on multimedia systems your task is to pick a specific topic from this field and study,
assess, summarize and discuss related literature in your own paper. All in all your seminar paper should give an
overview on the state of the art for unaware readers. Note at that point that your seminar paper is assessed as a
scientific paper.

# Schedule

| Day         | Time | Place  | Topic                                                                                                                                                    | 
|:------------|:-----|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------|
| 07.03. 2023 | 8:30 | S.2.42 | Organisational, scientific publications ~ [slides](https://docs.google.com/presentation/d/1RUOAn-Tl3rpXIbVUJFERoCu1EoUWzEMeOe-y_hcHVfk/edit?usp=sharing) | 
| 14.03. 2023 | 8:30 | S.2.42 | What is research? Master's Thesis 101 ~ [slides](https://docs.google.com/presentation/d/1ynYMyuXsB22T8Eb9ir9za02t-c21FkpmR1A5dOOdAAs/edit?usp=sharing)   | 
| 21.03. 2023 | 8:30 | S.2.42 | Paper formatting and topic distribution ~ [slides](https://docs.google.com/presentation/d/1cyCOF7hcwLu6GC4fKIFJ5WBxtp2ZV60NRoEFXTqcXaM/edit?usp=sharing) | 
| 28.03. 2023 | 8:30 | S.2.42 | Start presentations                                                                                                                                      | 
| 09.05. 2023 | 8:30 | S.2.42 | Intermediate presentations                                                                                                                               | 
| 20.06. 2023 | 8:30 | S.2.42 | Final presentations                                                                                                                                      | 

# Modalities

* Taking part in the course is mandatory. If you cannot be, there send an email to the lecturer in time. Missing courses results in failing the course.
* Start, intermediate and final presentations are mandatory. Missing to give either of them results in failing the course.
* The final paper has to be sent in __July 31st, 2023__ latest. If you can't make the date ask for an extension in time.

# Topics
You can choose a topic from the list, or find your own topic in the context of multimedia systems and games, including the following conferences:
* [ACM Multimedia](https://dl.acm.org/conference/mm)
* [ACM MMSys](https://dl.acm.org/conference/mmsys)
* [ACM SIGGRAPH](https://dl.acm.org/conference/siggraph)
* [ACM CHI PLAY](https://dl.acm.org/toc/pacmhci/2022/6/CHI+PLAY)

# Your paper
Your task is to find work related to the topic you select, read and assess it, put it into context and describe relations, benefit etc. This is also called a comparative study. 

Your paper is a scientific article. It has to match scientific quality in terms of citations, argumentation, facts, readability, and so on.
Your submission has to follow the _ACM SIG Proceedings Style_. You can find it on [Overleaf](https://www.overleaf.com/latex/templates/acm-conference-proceedings-master-template/pnrfvrrdbfwt) or on the [authors' page](https://www.acm.org/publications/proceedings-template). You are strongly encouraged to use [LaTeX](https://www.latex-tutorial.com/tutorials/) and check your text using [Grammarly](https://app.grammarly.com/).

Your paper should have 4 pages in the given ACM style. That's around 3000-4000 words typically. 
Please note term paper format, code of conduct, and plagiarism policies apply as detailed below.

## Paper Format
- Orderly fulfillment of formal requirements:
    * A paper follows the _ACM SIG Proceedings Style_
    * A paper is coherently formatted
    * A paper has a bibliography referencing all employed sources, including but not limited to online videos, web pages, and code repositories
- Adequate reliance on research literature:
    * Selection of citations
    * Provision of a well-formatted bibliography
-	Reliance on theoretical resources
-	Critical reflection on self-made observations


## Code of Conduct and Plagiarism Policies

The [University of California Berkely](https://sa.berkeley.edu/conduct/integrity/definition) offers the following definition:

```md
Plagiarism is defined as use of intellectual material produced by another person without acknowledging its source, for example:
* Wholesale copying of passages from works of others into your homework, essay, term paper, or dissertation without acknowledgment.
* Use of the views, opinions, or insights of another without acknowledgment.
* Paraphrasing of another person’s characteristic or original phraseology, metaphor, or other literary device without acknowledgment.
```

For computer science classes this includes source code, video tutorials, and web pages used to create software for the course. It is mandatory to check for license compatibility, like (i) can it be used, (ii) how to attribute, and (iii) what license is implied by including this source. It is also mandatory to cite the sources in the report. Failing to do that counts as plagiarism.

Note that the [Code of Conduct of the AAU](https://www.aau.at/en/research/research-profile/good-academic-practice/) applies. Students caught plagiarising or violation of the code of conduct will be removed from class immediately.

# Presentations
Each of the participants has to give three presentations.

## Start presentation
Goal of the start presentation is that you show that you have grasped the topic and know where to start. Describe your topic, give examples, and give us an idea on what you want to do when. If you have done research on papers, applications, tools, etc. already, give us preliminary results.  

## Intermediate presentation
Goal of this presentation is to show the topic to your colleagues and to show your current status of the seminar paper. At this point you should have read and understood your starting papers and have gotten a feeling for the topic as a whole. You don't need to have significant text, but I expect you to have found and investigates up to 5 related papers. The presentation is scheduled to last 240 seconds in [PechaKucha](https://en.wikipedia.org/wiki/PechaKucha)-like style, so each of your 12 slides is up on the wall for 20 seconds. Make sure you have got your slide setup prepared for that.

## Final presentation
The final presentation should take 15 minutes (no more and no less) and should summarize your work done in the paper.

# Topics

## Quality of Experience
- Juluri, P., Tamarapalli, V., & Medhi, D. (2015). Measurement of quality of experience of video-on-demand services: A survey. IEEE Communications Surveys & Tutorials, 18(1), 401-418.
- Skowronek, J., Raake, A., Berndtsson, G., Rummukainen, O. S., Usai, P., Gunkel, S. N., ... & Toet, A. (2022). Quality of experience in telemeetings and videoconferencing: a comprehensive survey. IEEE Access.
- Balachandran, A., Sekar, V., Akella, A., Seshan, S., Stoica, I., & Zhang, H. (2013). Developing a predictive model of quality of experience for internet video. ACM SIGCOMM Computer Communication Review, 43(4), 339-350.

## Movement in VR
- Cmentowski, S., Kievelitz, F., & Krueger, J. H. (2022). Outpace Reality: A Novel Augmented-Walking Technique for Virtual Reality Games. Proceedings of the ACM on Human-Computer Interaction, 6(CHI PLAY), 1-24.
- Nilsson, N. C., Serafin, S., Steinicke, F., & Nordahl, R. (2018). Natural walking in virtual reality: A review. Computers in Entertainment (CIE), 16(2), 1-22.
- Cannavò, A., Calandra, D., Pratticò, F. G., Gatteschi, V., & Lamberti, F. (2020). An evaluation testbed for locomotion in virtual reality. IEEE Transactions on Visualization and Computer Graphics, 27(3), 1871-1889.

...