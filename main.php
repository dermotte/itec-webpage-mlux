<!--- ------------------- MAIN ------------------- -->

<div class="row">
    <div class="col-md-8">
        <blockquote>
            <p>There is a theory which states that if ever anybody discovers exactly what the Universe is for and why it
                is here, it will instantly disappear and be replaced by something even more bizarre and inexplicable.
                There
                is another theory which states that this has already happened.</p>
            <footer>Douglas Adams</footer>
        </blockquote>
    </div>
    <div class="col-md-4 hidden-sm hidden-xs" style="background-color: #63768A; border-radius: 10pt">
        <a href="http://itec.aau.at">
            <img class="img-responsive" src="images/aau-logo_transparent.png">
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h1>Assoc. Prof. Dr. DI Mathias Lux</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <p class="text-justify">
            Dr. Mathias Lux is Associate Professor at the
            Institute for Information Technology (ITEC) at Klagenfurt University. He is working on user intentions in
            multimedia retrieval and production, semantics in social multimedia systems, and interactive multimedia in
            the domain of video games. In his scientific
            career he has (co-)authored more than 150 scientific publications, serves in multiple program committees
            and as reviewer of international conferences, journals and magazines on a regular basis, and has (co-)organized multiple scientific
            events. Mathias Lux is also well known for the development of the award winning and popular open source
            tools <a href="http://www.semanticmetadata.net">Caliph &amp; Emir</a> and <a
                href="http://www.lire-project.net/">LIRE</a> for multimedia information retrieval. He has integrated image indexing and retrieval features in
            the popular Apache Solr search server and his system is for instance powering the WIPO Global Brand Database.
            At Alpen-Adria-Universität Klagenfurt he has co-founded the master program Game Studies and Engineering and actively contributes to the program
            as head of the curriculum committee and regular lecturer. He established a lively community of game developers and enthusiasts who meet at
            regular events and game jams in and around Klagenfurt. In the administration of the Alpen-Adria-Universität Klagenfurt, he serves as vice-chair
            of the senate since 2013.
        </p>

        <p class="text-justify">
            Mathias Lux received his M.S. in Mathematics 2004, his PhD in Telematics 2006 from Graz University of Technology, both with distinction. He received his Habilitation (venia docendi) from Alpen-Adria-Universität Klagenfurt in 2013. He worked in the industry on web-based applications for several small Carinthian and Styrian companies, then as a junior researcher at the Know-Center, a research centre for knowledge-based applications, in Graz. He was a research and teaching assistant at the Knowledge Management Institute (KMI) at the Graz University of Technology and in 2006 he started working in a post-doc position at Alpen-Adria-Universität Klagenfurt, at the Institute for Information Technology.
        </p>

        <p>
            <a href="http://www.itec.uni-klu.ac.at/~mlux/cv-mlux.pdf">Curriculum vitae - PDF version</a>
        </p>

    </div>

    <div class="col-md-2">
        <div class=" row text-center">
            <img src="images/Mathias_Lux_2016.jpg" class="img-thumbnail img-responsive"
                 title="Prof. Mathias Lux"
                 alt="Prof. Mathias Lux" style="margin: 10px; max-height: 320px; width: auto; ">
        </div>
    </div>

    <div class="col-md-4">
        <ul>
            <li> 1996 - 2004 Studies in Mathematics / Graz University of Technology</li>
            <li> June 2004 Finished studies in Technical Mathematics at Graz University of Technology with distinction
                (Dipl. Ing. ~ master degree).
            </li>
            <li> 1998 - 2001 Employee and contract worker in companies of the Styria Medien AG group. (Carinthia Online,
                Media Consult Austria and Netconomy)
            </li>
            <li> 2001 - 2006 Junior Researcher at the Know-Center</li>
            <li> Nov. 2006 finished doctoral studies with distinction.</li>
            <li> 2006 - 2013 Assistant professor at the Institute for Information Technology (ITEC) at Klagenfurt
                University
            </li>
            <li>Oct. 2013 Habilitation at Klagenfurt University</li>
            <li>Vice chairperson of the senate of Klagenfurt University from Oct 2013 to Sep 2022</li>
            <li> Currently Associate Professor at the Institute for Information Technology (ITEC) at Klagenfurt
                University</li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h2>Publications</h2>

        <p>
            Find a list of all my publications including links to the PDFs and BibTeX entries here:
        </p>

        <p>
            <a href="http://www.itec.uni-klu.ac.at/~mlux/publications.html">http://www.itec.uni-klu.ac.at/~mlux/publications.html</a>
        </p>

        <h2>Contact</h2>
        <ul class="list-unstyled">
            <li><span class="glyphicon glyphicon-phone"></span> Phone: +43 (463) two thousand seven hundred three-six-one-five</li>
            <li><span class="glyphicon glyphicon-phone-alt"></span> Fax: +43 (463) two thousand seven hundred 99 three-six-one-five</li>
            <li><span class="glyphicon glyphicon-map-marker"></span> Address: Universit&auml;tsstrasse 65-67, 9020
                Klagenfurt,
                Austria
            </li>
            <li><span class="glyphicon glyphicon-map-marker"></span> Office: E.2.59</li>
            <li><span class="glyphicon glyphicon-envelope"></span> Email: mathias [dot] lux [at] aau [dot] at</li>
            <li><span class="glyphicon glyphicon-star"></span> Consultation hours (by video call): Monday, 3-4 pm, note: request appointments via email.
            </li>
            <li><span class="glyphicon glyphicon-heart"></span> <a
                    href="http://www.itec.uni-klu.ac.at/~mlux/cv-mlux.pdf"
                    class="urlextern"
                    title="http://www.itec.uni-klu.ac.at/~mlux/cv-mlux.pdf"
                    rel="nofollow">Curriculum vitae</a> (long
                version, PDF)
            </li>
            <!--li><span class="glyphicon glyphicon-headphones"></span> Google Talk: ...</li-->
            <li><span class="glyphicon glyphicon-globe"></span> Link to this page: <a
                    href="http://www.itec.aau.at/~mlux/">http://www.itec.aau.at/~mlux/</a>
            </li>
        </ul>

    </div>
    <div class="col-md-8">
<!--        <h2>News</h2>-->

        <!--div id="rss-feeds"></div>
        <a class="twitter-timeline" data-theme="dark" data-chrome="transparent,nofooter,noborders" data-tweet-limit="3" href="https://twitter.com/mathiaslux?ref_src=twsrc%5Etfw">Tweets by mathiaslux</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script-->

    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center"><span class="glyphicon glyphicon-arrow-down"></span></div>
</div>
<!--p>
    Furthermore an incomplete list of my research related activities can be found here: <a
        href="/~mlux/wiki/doku.php?id=research:activities" class="wikilink1" title="research:activities">Activities</a>.
</p-->
