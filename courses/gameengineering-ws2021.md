# Game Engineering

The goal of this course is to introduce students to computer game engineering. This is
not necessarily a technical issue especially as game engineering involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing.

After this course students should be able to understand the concepts and roles within a game project with the
knowledge how a game project can be set up and run, as well as detailed knowledge on particular processes within the
project.

The practical course associated with the lecture is taking place as a game jam. Find more information on that on the [game jam website](http://itec.aau.at/gamejam/).

## Online, but How?

Due to the restrictions necessary to prevent the spread of COVID-19 we are going to teach all courses in Game Studies and Engineering online. Please make sure to join the GSE Discord Server to stay up to date [https://discord.gg/YnX6AKG](https://discord.gg/YnX6AKG)

The game engineering lectures take place on [twitch.tv](https://www.twitch.tv/dermotte_) and are available as recordings afterwards, either on Twitch or linked here on the web page.

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/1A1ykm_71vhvAbD6ItCYujRJ8eOPev8ksZM-fZjoejWA/edit?usp=sharing) ~ lecture: [Youtube](https://youtu.be/ChRykiDNdxc) or [download](https://www2.itec.aau.at/owncloud/index.php/s/5OXcsfwZL3IbSFs)
3. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/1sJ3pFj_MCY1iGyKxgQrRCf0ME6SVL75Zcc6krl30pmo/edit?usp=sharing) ~ lecture: [Youtube](https://youtu.be/nSzl_4UpJGo) or [download](https://www2.itec.aau.at/owncloud/index.php/s/2JAUfJ8vH50nIul)
2. Game Projects: Documents and Pitches ~ [slides](https://docs.google.com/presentation/d/1sJ3pFj_MCY1iGyKxgQrRCf0ME6SVL75Zcc6krl30pmo/edit?usp=sharing) ~ lecture: [Youtube](https://youtu.be/8D-ZbYpilNc) or [download](https://www2.itec.aau.at/owncloud/index.php/s/tdDGAe1wLr61KiT)
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/15YbD4LLRIpwhlOomM3thijAGpY26LwRaYZTIt03fjwg/edit?usp=sharing) ~ lecture: [Youtube](https://youtu.be/cpPNW0zaP7U) or [download](https://www2.itec.aau.at/owncloud/index.php/s/D3wAR33vYlobWpA)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1ALOndeNm3N7SaAU3yUDFXiQ-HJ_xm8mJThSEpcJ7nAs/edit?usp=sharing) ~ lecture: [Youtube](https://youtu.be/s3tlS9m6Pco) or [download](https://www2.itec.aau.at/owncloud/index.php/s/5hshTTPtuSZ4dGn)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1BFeVs3X0dTLlDVTDDoauh0n72JczvndHEm9DO6eu5Wk/edit?usp=sharing) ~ lecture [Youtube part 1](https://youtu.be/l6bYkZJoVPI), [Youtube part 2](https://youtu.be/SDRmL0p3eX8) or [download part 1](https://www2.itec.aau.at/owncloud/index.php/s/vMU4WQABqKHcMxH), [download part 2](https://www2.itec.aau.at/owncloud/index.php/s/c0GdWUFVaZzL7rF)
7. Playtests ~ [slides](https://docs.google.com/presentation/d/1Z_TL_t5w1qIisWPJrk9ENlzLJHAyvi4X_M2j22iu5Bw/edit?usp=sharing) ~ lecture [Youtube](https://youtu.be/ZO1jFdc1kpc) or [download](https://www2.itec.aau.at/owncloud/index.php/s/6DCVIw82bQOKd6R)
6. Animation ~ [slides](https://docs.google.com/presentation/d/1H29Mwk5X3bNdULU4kYV9mfvRqwl2TN3BNLbogEH1W9s/edit?usp=sharing) ~ lecture [Youtube](https://youtu.be/dKjIZ3JnfI4) or [download](https://www2.itec.aau.at/owncloud/index.php/s/WtxhLeJIcIKVQOx)
8. Sound & Interactive Music ~ [slides](https://docs.google.com/presentation/d/1KalOzA7AjnNcuIb0DJYPZ60N9a4S9PJGiw9vr0KU1GE/edit?usp=sharing) ~ lecture [Youtube](https://youtu.be/o89fSwO7jxo) or [download](https://www2.itec.aau.at/owncloud/index.php/s/wpWCFGU6LUFwDcQ)

## Modalities

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The open book exam will be online, take 30 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://drive.google.com/open?id=1PF0CIe6nT5-dmRtOhRKg5nrlPFZfr6ikoH1xQHE22iU).

Please check if you can access the [Moodle page](https://moodle.aau.at/course/view.php?id=30147). The exam will be available on Jan 25 at 10am CET for an hour. As soon as you start, you have 30 minutes to complete it.

## Links
Here are some links to dive further into the topics of the course.

### Programming
- [Hour of Code](https://code.org) - Learn to program with a visual programming lecture
- [Scratch](https://mit.scratch.edu) - Use the same visual programming lecture to create interactive programs and games

### Prototyping
- [Twine](https://twinery.org/) - An open-source tool for telling interactive, nonlinear stories.
- [Ren'Py](https://www.renpy.org/) - A visual novel engine

### Informative and fun videos
- [Extra Credits](https://www.youtube.com/user/ExtraCreditz) - Covers many basic aspects of game dev
- [GDC Vault](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ) - Full length presentations from GDC
