# Interaktive Multimediasysteme B (Computer Games)

The goal of this course is to introduce students to computer game production. This is
not necessarily a technical issue especially as game production involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing.
The course has already taken place in previous years, so you might take a look at the
[2020 web page](?id=courses/games-ss20.md). The lecture is given in German,
but slides and homepage are written in English. For assignments you can choose between German and English language.

## Modalities
The course aims to teach in a practical as well as in a theoretical way. Throughout the
lecture you will have (i) readings, (ii) practical assignments, (iii) a final exam and (iv) a final game project. The readings
are specific to the lecture topic and will be referenced from the web page. Make sure you read (or watch or play)
them. Questions might pop up in the final exam. The final grade is based on the (i)
assignments, (ii) the final project and a (iii) final exam.

* Readings - For the course it's mandatory to read (or view, in case of videos, or even play in case of games) the readings. They will be announced throughout the course in the schedule table.
* Practical assignment - Two practical assignments prior to the final project.
* Written exam - A final, short exam on the course topics at the end of the course.     
* Final project - Create a game in a group, more information see below - [Catalogue of Questions](https://docs.google.com/document/d/1Svj9aGtjdiXSUcBzJvNer7O8ax48z1QbaZZoVAjwXS8/edit?usp=sharing).

## Schedule

All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below). Due to the situation around COVID-19 all lectures take place online. After the online courses, lecture recordings are available.

After the course you can find me in the _Lectures Q&A_ audio channel in the [GSE Discord Server](https://discord.gg/YnX6AKG).

| Day    | Time   | Place  | Topic | Readings |
|:------|:------|:------|:---|:---|
| Mo, 08.3.  | 14-16 | [Youtube](https://youtu.be/FrfV5O5PPuk) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/aFFA6TX0Vd7EdRj) | Introduction and motivation ~ [slides](https://docs.google.com/presentation/d/1dathDo-Po6KAVQqraHNir_EN_2Py0f9dqgI4xVnpo58/edit?usp=sharing) | [Game - AI Dungeon](https://aidungeon.io/) |
| Mo, 15.3.  | 14-16 | [Youtube](https://youtu.be/RTvGzod2kok) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/uEgRlTg6XBgnuml) | Business of Games ~ [slides](https://docs.google.com/presentation/d/1LX8smG5tG1fyjUxYNEFRTey6P2zUB6Wg3_5NSpOAPTw/edit?usp=sharing) | [Video - Free to Play](http://store.steampowered.com/app/245550/?snr=1_7_7_151_150_1) |
| Mo, 22.3.  | 14-16 | [Youtube](https://youtu.be/y_bh2UtYpbg) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/ye8TR2ymNKgJ961) | Game Loop ~ [slides](https://docs.google.com/presentation/d/1g1GFB01nXpeZRSttE-YPPN79c-Q_pbFG4inwbO144bQ/edit?usp=sharing)  | [Game - Paperclip](https://www.decisionproblem.com/paperclips/index2.html) |
| Mo, 12.4.  | 14-16 | [Youtube](https://youtu.be/4agr4NHQiko) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/P3l0B8ayjBl9CG5) | 2D Graphics ~ [slides](https://docs.google.com/presentation/d/1r_kX9mSEl-ebf1vi6Vbd1GhL9e2EZYesroE6OPXvrdY/edit?usp=sharing)| [Video - Postmortem Dying Light](https://youtu.be/UJgTboToOg8) |
| Mo, 19.4.  | 14-16 | [Youtube](https://youtu.be/GRwz-he7zSo) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/KtGKln6uasaSQka) | Game Design ~ [slides](https://docs.google.com/presentation/d/1cNrEkk8B_CLMvspR4XBHBkEt_n8BC4TicFdsAOSiENI/edit?usp=sharing)  |  [Level Design Workshop: Designing Celeste](https://www.youtube.com/watch?v=4RlpMhBKNr0) |
| Mo, 26.4.  | 14-16 | [Youtube](https://youtu.be/_t0ChZ2XwZ8) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/anQJMgdMVE3cani) | Input and Sound ~ [slides](https://docs.google.com/presentation/d/18GkXBcOSXQnU1-UOQH_6qo_IW0vhOzPscVenD9fETHw/edit?usp=sharing) |  Try 2 or more [klujam games](https://itch.io/jam/7thklujam) |
| Mo, 03.5.  | 14-16 | [Youtube](https://youtu.be/tqoDoLX-RFk) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/MeTl7Tlqw2xAwvq) | Game Production and Development ~ [slides](https://docs.google.com/presentation/d/1QKZ3dqWc9MRjHmcLZh6keSS3nO1z-wFMUY9GwhKrYN4/edit?usp=sharing) |  [Video - Juice It or Loose It](https://www.youtube.com/watch?v=Fy0aCDmgnxg) |
| Mo, 10.5.  | 14-16 | [Youtube](https://youtu.be/bWxh_fDS18M) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/mwVoTPx2dS9Q8Ll) | Introduction to 3D Graphics ~ [slides](https://docs.google.com/presentation/d/1n6_9LW4vH9j3HQch26JnTYuiVtqTwM_7N-BPql5hR5M/edit?usp=sharing) | [Video - Audio Bootcamp: Dialogue 101](https://www.youtube.com/watch?v=W7-gIHOOues) |
| Mo, 17.5.  | 14-16 | [Youtube](https://youtu.be/xgFIIY4Sn2A) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/L1SZRg767v1mOvE) | Game AI ~ [slides](https://docs.google.com/presentation/d/1Xn8ch6jAo3NOlKmxgsDmqha3-6KI3AUxl-OOAanbpig/edit?usp=sharing) |  [Video - How Players Play Games: Observing the Influences of Game Mechanics](https://www.youtube.com/watch?v=dBzsG-k6-G4) |
| Mo, 31.5.  | 14-16 | [Youtube](https://youtu.be/3H89JF-Enqw) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/D0JAbYaCWJQy4b0) | Game Physics ~ [slides](https://docs.google.com/presentation/d/1y5knjshSRN-YiB4I3ArwWg_87SR118LjeW5ecaHjRiE/edit?usp=sharing)|  ... |
| Mo, 07.6.  | 14-16 | [Youtube](https://youtu.be/oF3GvK2mQ1w) or [Download](https://www2.itec.aau.at/owncloud/index.php/s/FVamJFxOz9r9M0t) | History of Games ~ [slides](https://docs.google.com/presentation/d/1_iH0DDRFyeG897nIdYJVNwhGm8qlg-gcjinjKDC-3oY/edit?usp=sharing) |  ... |
| Mo, 14.6.  | 14-16 | AAU Classroom | Student Projects: Presentations I |  none |
| Mo, 21.6.  | 14-15.30 | AAU Classroom | Student Projects: Presentations II |  none |
| Mo, 21.6.  | 15.30-16 | AAU Classroom | Final Exam (online) - [Catalogue of Questions](https://docs.google.com/document/d/1Svj9aGtjdiXSUcBzJvNer7O8ax48z1QbaZZoVAjwXS8/edit?usp=sharing) |  none |

## Game Jam
You are strongly encouraged to take part in the game jam, May 28-30, 2021. You can join any group there and the game developed at the game jam can be presented as final project.

## Mandatory Exercise 01 - Pong with Löve
For the first mandatory exercise you are to create a Pong clone with [Löve2D](https://love2d.org/). It does not need to be an accurate copy (graphically or otherwise), but just let two people play a game of pong and count the points. Your Pong clone should fulfil the following requirements:

1. Submit as runnable .love file
1. Use `love.graphics.rectangle(...)` to paint the bars and the ball
1. Control by keyboard

Submit your Pong clone, preferrably as a .love file, to Mathias Lux via e-mail until **April 18th, 2021, midnight**.

Are you not challenged enough? - Use the [Windfield physics wrapper for Löve2D](https://github.com/adnzzzzZ/windfield) to create a pong version with a physics engine in the back end. Make sure to reduce friction of the ball and set the position of the paddles in the update callback function ;)   

## Mandatory Exercise 02 - Fun with Godot
For the second mandatory exercise you are to create a game, where you land rocket on a small plateau, with [Godot](https://godotengine.org/).  Your game should fulfil the following requirements:

1. Use the built-in Godot physics engine with the rocket / space ship being a _RigidBody2D_ and the plateau being a _StaticBody2D_
1. Use the arrow keys to rotate and fire up the rocket
1. Let the game be similar to the example: [play in browser](http://www.itec.uni-klu.ac.at/~mlux/games/cg2020/) or [download for windows](http://www.itec.uni-klu.ac.at/~mlux/games/cg2020/example-game-windows.zip)
1. Submit as zipped Godot Project.

Submit your game, the zipped project folder, to Mathias Lux via e-mail until **May 2nd, 2021, midnight**.

## Final Project
Your final project can be done in a group with up to three people. To finish the project you need to:
1. Hand in the project & the documentation
    1. link to the project (ie. itch.io, Github, WeTransfer, ...)
    1. game concept document (1 page)
    1. post mortem (1 page)
    1. presentation slides
1. Present the project in the lecture on June 15 or June 21, 2020 (6 minutes presentation, 4 minutes Q&A)
    1. list the group members
    1. explain the concept
    1. show the game
    1. talk about the lessons learned (post mortem)

The project can be a stand-alone game, a level for a game, or a mod for a game. If you attended the game jam, please present your game jam project and just create the documents and slides.
