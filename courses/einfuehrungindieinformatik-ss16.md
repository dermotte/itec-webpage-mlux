#  Einf&uuml;hrung in die Informatik - SS 2016

## Zwischenergebnisse
Die Zwischenergebnisse werden nach dem ersten Minitest hier ver&ouml;ffentlicht. Aufgaben und Modalitäten finden Sie im [Moodle Kurs](https://campus.aau.at/studien/ctl/elearning/gotoELearning?rlvkey=83244).

## Zus&auml;tzliche Informationen
* [AAU Kursbeschreibung](https://campus.aau.at/studien/lvkarte.jsp?sprache_nr=35&rlvkey=83244)
* [Moodle Kurs](https://campus.aau.at/studien/ctl/elearning/gotoELearning?rlvkey=83244)
