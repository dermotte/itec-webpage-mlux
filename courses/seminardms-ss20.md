# Seminar Distributed MM Systems (2020)
In this seminar on distributed multimedia systems your task is to pick a specific topic from this field and study,
assess, summarize and discuss related literature in your own paper. All in all your seminar thesis should give an
overview on the state of the art for unaware readers. Note at that point that your seminar paper is assessed as a
scientific paper.

# Schedule
Due to the situation around CoViD-19 all besides the first lecture take place online. After the online courses, video lectures are available. 

| Day    | Time   | Place  | Topic | 
|:------|:------|:------|:---|
| 11.03. 2020   | 2pm   | Online  | Initial meeting &amp; [Topics](https://docs.google.com/document/d/1Ttc4K4ZnHb6rifc4N3J1F4FXmHlVPzHZ-DHrSoACdGE/edit?usp=sharing)| 
| 23.03. 2020   | 4pm   | [Video Recording](https://youtu.be/ujubRIYPx6k)  | LaTeX, Template, Research Q&A ~ [slides](https://docs.google.com/presentation/d/1t29X4sEPA4fySfnpinD650Ou9Jjc8ifGrTWnwkWOvPQ/edit?usp=sharing) |
| 27.04. 2020   | 4pm   | Online   | Start presentations |
| 25.05. 2020   | 4pm   | Online   | Intermediate presentations |
| 29.06. 2020   | 5:30 pm  | Online   | Final presentations |

# Online - FAQ

For online sessions we are using https://meet.jit.si/. It's an open source Web-RTC based video conferencing tool, where you can also share your screen or a Chrome tab. I strongly recommend to have the latest version of [Chrome](https://www.google.com/chrome/), [Brave](https://brave.com/), or [Firefox](https://www.mozilla.org/) running. You can click on the links in the schedule table above any time and test if camera and microphone work for your setup.  

If you want to use Jitsi to present slides, a good way to do this is to connect twice to the same chat room. With one connection you stream your webcam and audio, the other connection casts a browser tab with the slides. Make sure the second connection is muted and has the volume turned off. 

# Modalities

* Taking part in the course is mandatory. If you cannot be, there send an email to the lecturer in time. Missing courses results in failing the course.
* Start, intermediate and final presentations are mandatory. Missing to give either of them results in failing the course.
* The final paper has to be sent in __July 31st, 2020__ latest. If you can't make the date ask for an extension in time.

# Your paper
Your task is to find work related to the the topic you select, read and assess it, put it into context and describe 
relations, benefit etc. This is also called a comparative study. 

Your paper is a scientific article. It has to match scientific quality in terms of citations, argumentation, facts, readability, and so on.
Your submission has to follow the _ACM SIG Proceedings Style_. You can find it on [Overleaf](https://www.overleaf.com/latex/templates/acm-conference-proceedings-master-template/pnrfvrrdbfwt) or on the [authors' page](https://www.acm.org/publications/proceedings-template). You are strongly encouraged to use [LaTeX](https://www.latex-tutorial.com/tutorials/) and check your text using [Grammarly](https://app.grammarly.com/).

Your paper should have 5000 words. That's around 6 pages in the given ACM style.

# Presentations
Each of the participants has to give three presentations.

## Start presentation
Goal of the start presentation is that you show that you have grasped the topic and know where to start. Describe your topic, give examples, and give us an idea on what you want to do when. If you have done research on papers, applications, tools, etc. already, give us preliminary results.  

## Intermediate presentation
Goal of this presentation is to show the topic to your colleagues and to show your current status of the seminar paper. At this point you should have read and understood your starting papers and have gotten a feeling for the topic as a whole. You don't need to have significant text, but I expect you to have found and investigates up to 5 related papers. The presentation is scheduled to last 240 seconds in [PechaKucha](https://en.wikipedia.org/wiki/PechaKucha)-like style, so each of your 12 slides is up on the wall for 20 seconds. Make sure you have got your slide setup prepared for that.

## Final presentation
The final presentation should take 15 minutes (no more and no less) and should summarize your work done in the paper.

# Topics

...