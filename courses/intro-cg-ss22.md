# Introduction to Computer Graphics

This course aims to give a basic understanding of computer graphics and its role in computer games.
Topics will be a math primer, 2D and 3D graphics, shading, and hands on with practical tools.
Grading is based on (i) mandatory assignments, (ii) a final project, and (iii) a final report

## Schedule

All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below). Due to the situation around CoViD-19 we start the summer term 2022 with online lectures and see where it leads us. After the online courses, lecture recordings are available on [YouTube](https://www.youtube.com/playlist?list=PLkb7TymgoWW4ADg6BakZ4va4utZbP-3iy) and for [download](https://www2.itec.aau.at/owncloud/index.php/s/H9glFm1LXXXxRZn). Live lectures are taking place on the Twitch Channel [https://www.twitch.tv/dermotte_](https://www.twitch.tv/dermotte_).

After the course you can find me in the _Lectures Q&A_ audio channel in the [GSE Discord Server](https://discord.gg/YnX6AKG).

1. Introduction ~ [slides](https://docs.google.com/presentation/d/1TjtfhJoBxQuo0RXr_znkCLTC-eHI7VBo2JPXobhfymg/edit?usp=sharing) ~ readings:  [Sand rendering in Journey](https://www.youtube.com/watch?v=wt2yYnBRD3U)
2. Math 101 ~ [slides](https://docs.google.com/presentation/d/1M-yWUpQJh3EVycB-1yT9bGJlAjrHGgHZQ_9p6HZK9a4/edit?usp=sharing) ~ readings: [p5js tutorial video](https://www.youtube.com/watch?v=8j0UDiN7my4)
3. Coordinates and Transformations ~ [slides](https://docs.google.com/presentation/d/1cq_SL1jxhbZojqU2mrknrcVSG5LT7J53_guBCQSno8k/edit?usp=sharing) ~ readings:  [Easing functions](https://www.youtube.com/watch?v=mr5xkf6zSzk)
4. Meshes and Lights ~ [slides](https://docs.google.com/presentation/d/1XHKQ2ryqfJtPbYgFP9XA6ZumD1VaK_o7UQgmVyATXlM/edit?usp=sharing) ~ readings:  [Pixar in a Box: The Art of Lighting](https://www.youtube.com/playlist?list=PLkb7TymgoWW4iwQI1meWIS4dOpVAqlPMX) and [Three Point Lighting](https://youtu.be/Bmme1slWdm4)
5.  Shading  ~ [slides](https://docs.google.com/presentation/d/1EdS_mGdYCM6mPXVWpUPC9Zkkw8lkNejO2whgN0JqmXg/edit?usp=sharing) ~ readings:  [Math for Game Programmers: Juicing Your Cameras With Math](https://www.youtube.com/watch?v=tu-Qe66AvtY)
6. Cameras ~ [slides](https://docs.google.com/presentation/d/1nJqbyroTFAKuyu69nJC5EoZldpE6Ykks_0IvxYFZ0TU/edit?usp=sharing) ~ readings: [Creating an Emotionally Engaging Camera for Tomb Raider](https://www.youtube.com/watch?v=doVivf-Nvuo) |
7. Textures  ~ [slides](https://docs.google.com/presentation/d/1N9RbLFHhurAH6skdcOskdYbmQ0qcqQJVfRgzSUOwUZY/edit?usp=sharing)
8. Blender Workshop ~ [slides](https://docs.google.com/presentation/d/1mAOYp4CPmvrtL3oOwXuqZDl8vGKPRwJRzivBAxYAlEM/edit?usp=sharing) ~ readings:  [Blender Guru - Beginners Tutorial Part 1](https://youtu.be/TPrnSACiTJ4) and [Blender Tutorial - Easy Low Poly Environment in Eevee](https://youtu.be/_gyts71XMtw)

### References and Links
 1. [p5js](https://p5js.org/) - JavaScript based implementation for the first part of the course
 1. [Processing](https://processing.org/) - Java based implementation for the first part of the course
 1. [Blender Tutorials](https://www.blender.org/support/tutorials/) - Learn Blender on your own
 1. [Blender Step-By-Step Videos](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6) - It's for Blender 2.8, but it's still valid
 1. [CC0 Textures](https://cc0textures.com/) - CC0 licensed high quality textures
 1. [Thingiverse](https://www.thingiverse.com/) - Database of printable 3D objects
 1. [HDRI Haven](https://hdrihaven.com/) - CC0 licensed high quality HDRIs
 1. [Kenney Assets](https://www.kenney.nl/assets) - CC0 licensed assets that can be used in Blender

## Exercises (Mandatory Assignments)

Please upload the exercises (just the .js or the .pde file and eventual texture or model files) to Moodle until the end of the announced day.

| ID | Title | Slide | Deadline | Example Solution |
|:---|:---|:---|:---|:---|
| EX 01 | Vector Reflection | slide # 32 in Math 101 | March 31, 2022 | ... |
| EX 02 | LERP Game | slide # 44 in Math 101 | April 7, 2022 | ... |
| EX 03 | Rotating Boxes | slide # 50 in Coordinates and Transformations | April 21, 2022 | ... |
| EX 04 | Lighting a Scene | slide # 36 in Meshes and Lights | April 28, 2022 | ... |
| EX 05 | Bounded Camera | slide # 16 in Cameras | May 19, 2022 | ... |
| EX 06 | Textured Die | slide # 29 in Textures | June 9, 2022 | ... |


## Final Project
Please prepare your final project until **June 23, 2022**. Goal of the final project is to apply what you learned in the lecture in a practical way. Basic idea is to model a scene in Blender and render it. However, if this does not challenge you enough, then pick a topic and a challenge in the field of computer graphics, and get creative with a tool of your choice. This can for instance be a 3D game, a demo in a game engine, or a extensive dive into shader programming.

For the presentation (max duration 3 minutes) on June 23, 2022 make sure you can

1. show your result,
2. outline your work (contribution), and
3. discuss the lessons learned and obstacles encountered.

Please also make sure to deliver the following items in a ZIP file via upload on Moodle until **June 23, 2022**

1. Your work as source code or a link to Github, Bitbucket, etc.
2. Your result as rendering (images, movie) or executable (eg. Windows or web)
3. Your slides from the presentation
4. A PDF work report with a (i) cover page including your name and student number, (ii) a main text body of around 800 words (2 pages) detailing what you did, how you did it, and what you learned.

## How to get a positive grade?
Please make sure to fulfil the requirements to the letter. As the current situation requires to do everything online, I cannot build upon my communication with you in the classroom. If you cannot meet the requirements for a positive grade, please let me know before the deadlines, e.g. tell me that you are not able to submit an exercise before it is due.

1. Hand in all assignments in time
2. Do and present the final project
3. Submit your project with a report

Each of the above makes up a third of your grade, but you have to submit them all to get a positive grade.

## Additional Materials

All the materials from the course are available in the Github repo at [https://github.com/dermotte/cg-lecture-2021](https://github.com/dermotte/cg-lecture-2021).
