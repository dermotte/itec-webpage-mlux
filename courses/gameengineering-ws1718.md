# Game Engineering

The goal of this course is to introduce students to computer game engineering. This is
not necessarily a technical issue especially as game engineering involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing. 

After this course students should be able to understand the concepts and roles within a game project with the 
knowledge how a game project can be set up and run, as well as detailed knowledge on particular processes within the 
project.

The practical course associated with the lecture is taking place as a game jam. Find more information on that on the [winterjam website](http://itec.aau.at/gamejam/).

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/1gq36I_BVbW4B2TLZEPpEMwaJpoZVtgYpkqiJ6-GkLxw/edit?usp=sharing)
2. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/10jRi2_16vJsUA9Pep1yJH5D7r-kortqQ-n8Sm57kRK4/edit?usp=sharing)
3. Game Projects: Documents and Pitches ~ slides see above
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/1YOnjuVPic_SOPM0VhS1bg3UoXWg0hfG0oHdjPSqORIo/edit?usp=sharing)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1D-_TloP_USj_kUa1fAoj8jXd-aynt7vqRNYscCzQc4s/edit?usp=sharing)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1Vob4RW38vI4-5vJl6zd4nkfp8KiZKWswr0oTZGDv5U8/edit?usp=sharing)
7. Playtests ~ [slides](https://docs.google.com/presentation/d/1xLZXJQH4qCwsECIpAfo3v018XZfaywklNM6F3dDAB-w/edit?usp=sharing)
6. Hands On with Game Engines ~ [slides](https://docs.google.com/presentation/d/1vYY5uenjHHvdmhvIO4g_M8-kkxBSd4RNFehtbcZr62U/edit?usp=sharing)
8. Toolchain

## Modalities 

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The exam will be in written 
form, take 50 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://docs.google.com/document/d/10s1Q3ZR9bsy2yjPYxuoa1eacreRIIz7vbfxF6ay8K5I/edit?usp=sharing). 

## Links

- [Hour of Code](https://code.org)
- [Scratch](https://mit.scratch.edu)
- [Google Blockly](https://developers.google.com/blockly/)