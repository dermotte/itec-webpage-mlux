# Introduction to Computer Graphics

This course aims to give a basic understanding of computer graphics and its role in computer games.
Topics will be a math primer, 2D and 3D graphics, shading, and hands on with practical tools.
Grading is based on (i) mandatory assignments, (ii) a final project, and (iii) a final report

## Schedule

All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below). Due to the situation around CoViD-19 all lectures take place online. After the online courses, lecture recordings are available. Live lectures are taking place on the Twitch Channel [https://www.twitch.tv/dermotte_](https://www.twitch.tv/dermotte_).

After the course you can find me in the _Lectures Q&A_ audio channel in the [GSE Discord Server](https://discord.gg/YnX6AKG).

| Day    | Time   | Place  | Topic | Readings |
|:------|:------|:------|:---|:---|
| Do, 04.3.  | 13:00-14:30 | [YouTube](https://youtu.be/HGK7uk82qYA) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/o3XgIzBPexYZ1wq) | Introduction ~ [slides](https://docs.google.com/presentation/d/1jYbNgP9cNsM8VGe9yhJOynnrasTbjuSFqLg5dOE3cR4/edit?usp=sharing) |  [Sand rendering in Journey](https://www.youtube.com/watch?v=wt2yYnBRD3U) |
| Do, 11.3.  | 13:00-14:30 | [YouTube](https://youtu.be/_pbju0JLWCo) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/hDhKFk54r4lwYsy) | Math 101 ~ [slides](https://docs.google.com/presentation/d/1AOAeeaW1-FTdL0pFCMDzZpPccLDtLgMyd6nBwLliZcQ/edit?usp=sharing)|  [Easing functions](https://www.youtube.com/watch?v=mr5xkf6zSzk) |
| Do, 18.3.  | 13:00-14:30 | [YouTube](https://youtu.be/0AtTEoz9924) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/i3PPcPCmMXOjXoq) | Coordinates and Transformations ~ [slides](https://docs.google.com/presentation/d/1WWj0B6lDPHY0id-79rTsTqQQUoQiGRS01XfKwabAhN8/edit?usp=sharing) |  [Stable Elasticity from Pixar (just the video)](http://graphics.pixar.com/library/StableElasticity/) |
| Do, 25.3.  | 13:00-14:30 | [YouTube](https://youtu.be/xzmaKWHLAlo) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/AhrKwCJFSerJYyQ) | Meshes and Lights ~ [slides](https://docs.google.com/presentation/d/1w5SIGO83UayAlYveVtaSOR1BDke3PtNpwbtzQlMcXgs/edit?usp=sharing) |  [Pixar in a Box: The Art of Lighting](https://www.youtube.com/playlist?list=PLkb7TymgoWW4iwQI1meWIS4dOpVAqlPMX) and [Three Point Lighting](https://youtu.be/Bmme1slWdm4)  |
| Do, 15.4.  | 13:00-14:30 | [YouTube](https://youtu.be/b0rb90Sb0E0) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/yN3a0G0qVzu2HMj) | Shading  ~ [slides](https://docs.google.com/presentation/d/16Ey_mK2Y2pJQwlNEpGmmaYX6bfAOZG3H8IXwL14R2c4/edit?usp=sharing) |  [Math for Game Programmers: Juicing Your Cameras With Math](https://www.youtube.com/watch?v=tu-Qe66AvtY) |
| Do, 22.4.  | 13:00-14:30 | [YouTube](https://youtu.be/FjBKV2_OQSQ) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/tpDJ0KHTLJqRvPO) | Cameras ~ [slides](https://docs.google.com/presentation/d/1Wm-w8TCFoPGtJNvIYvy4kstMc5uK_9GvyvKIBsTVrI4/edit?usp=sharing)  |  [Creating an Emotionally Engaging Camera for Tomb Raider](https://www.youtube.com/watch?v=doVivf-Nvuo) |
| Do, 29.4.  | 13:00-14:30 | [Youtube](https://youtu.be/sKasYzZ7YJA) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/5EANBWDaMkbUDE0) | Textures  ~ [slides](https://docs.google.com/presentation/d/1pQI1mTKIIZhz1j2VgTpsh1RcW5PprKGQN-Yi_o7mLAs/edit?usp=sharing) |  none |
| Tu, 06.5.  | 13:00-14:30 | [Youtube](https://youtu.be/05wFRwl4Ss0) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/cyeOQUEVq58KUoO) | VR & the uncanny valley ~ [slides](https://docs.google.com/presentation/d/14yulYNrU11NonLpAAnRppYyK-Bg42XR8hmbuaWUIsl0/edit?usp=sharing) |  none |
| Do, 20.5.  | recorded | [Youtube](https://youtu.be/eW2PZb8VGho) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/JzPhFsx3NBhkCqW) | Blender Fundamentals Part 1 ~ [slides](https://docs.google.com/presentation/d/1oFfn77Mmvx7z_AMmnkQfASMnGbTQMGcShaLcSGgqUdo/edit?usp=sharing) |  [Blender Guru - Beginners Tutorial Part 1](https://youtu.be/TPrnSACiTJ4) |
| Do, 27.5.  | recorded | [Youtube](https://youtu.be/YXHusBi2B7I) ~ [download](https://www2.itec.aau.at/owncloud/index.php/s/ST59miT1FJFnRzA) | Blender Fundamentals Part 2 |  [Blender Tutorial - Easy Low Poly Environment in Eevee](https://youtu.be/_gyts71XMtw) |
| Do, 10.6.  | 13:00-13:30 | [Discord](https://discord.gg/YnX6AKG) | Q&A |  none  |
| Do, 17.6.  | 13:00-14:30 | [Discord](https://discord.gg/YnX6AKG) | Student Presentations |  none |
| Do, 24.6.  | 13:00-14:30 | [Discord](https://discord.gg/YnX6AKG) | Student Presentations |  none |


### References and Links
 1. [p5js](https://p5js.org/) - JavaScript based implementation for the first part of the course
 1. [Processing](https://processing.org/) - Java based implementation for the first part of the course
 1. [Blender Tutorials](https://www.blender.org/support/tutorials/) - Learn Blender on your own
 1. [Blender Step-By-Step Videos](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6) - It's for Blender 2.8, but it's still valid
 1. [CC0 Textures](https://cc0textures.com/) - CC0 licensed high quality textures
 1. [Thingiverse](https://www.thingiverse.com/) - Database of printable 3D objects
 1. [HDRI Haven](https://hdrihaven.com/) - CC0 licensed high quality HDRIs
 1. [Kenney Assets](https://www.kenney.nl/assets) - CC0 licensed assets that can be used in Blender

## Exercises (Mandatory Assignments)

Please send the exercises (just the .js or the .pde file and eventual texture or model files) to M. Lux via [email](http://www.itec.uni-klu.ac.at/~mlux/) until the end of the announced day.

| ID | Title | Slide | Deadline | Example Solution |
|:---|:---|:---|:---|:---|
| EX 02-01 | Vector Reflection | #32 in [02](https://docs.google.com/presentation/d/1AOAeeaW1-FTdL0pFCMDzZpPccLDtLgMyd6nBwLliZcQ/edit?usp=sharing) | Apr 18, 2021 | ... |
| EX 02-02 | The Lerp Game |  #44 in [02](https://docs.google.com/presentation/d/1AOAeeaW1-FTdL0pFCMDzZpPccLDtLgMyd6nBwLliZcQ/edit?usp=sharing) | Apr 18, 2021 | ... |
| EX 03-01 | Two Boxes |  #50 in [03](https://docs.google.com/presentation/d/1WWj0B6lDPHY0id-79rTsTqQQUoQiGRS01XfKwabAhN8/edit?usp=sharing) | Apr 25, 2021 | ... |
| EX 04-01 | Light a Scene |  #36 in [04](https://docs.google.com/presentation/d/1w5SIGO83UayAlYveVtaSOR1BDke3PtNpwbtzQlMcXgs/edit?usp=sharing) | April 25, 2021 | ... |
| EX 06-01 | Bounded Camera |  #16 in [06](https://docs.google.com/presentation/d/1Wm-w8TCFoPGtJNvIYvy4kstMc5uK_9GvyvKIBsTVrI4/edit?usp=sharing) | May 16, 2021 | ... |
| EX 07-01 | Six-Sided Die |  #30 in [07](https://docs.google.com/presentation/d/1pQI1mTKIIZhz1j2VgTpsh1RcW5PprKGQN-Yi_o7mLAs/edit?usp=sharing) | May 23, 2021 | ... |

## Final Project
Please prepare your final project until **June 17, 2021**. Goal of the final project is to apply what you learned in the lecture in a practical way. Basic idea is to model a scene in Blender and render it. However, if this does not challenge you enough, then pick a topic and a challenge in the field of computer graphics, and get creative with a tool of your choice. This can for instance be a 3D game, a demo in a game engine, or a extensive dive into shader programming.

For the presentation on June 17, 2021 make sure you can

1. show your result,
2. outline your work (contribution), and
3. discuss the lessons learned and obstacles encountered.

Please also make sure to deliver the following items in a ZIP file via upload on [Moodle](https://moodle.aau.at/course/view.php?id=31660) until **June 27, 2021**

1. Your work as source code or a link to Github, Bitbucket, etc.
2. Your result as rendering (images, movie) or executable (eg. Windows or web)
3. Your slides from the presentation
4. A PDF work report with a (i) cover page including your name and student number, (ii) a main text body of around 800 words (2 pages) detailing what you did, how you did it, and what you learned.

## How to get a positive grade?
Please make sure to fulfil the requirements to the letter. As the current situation requires to do everything online, I cannot build upon my communication with you in the classroom. If you cannot meet the requirements for a positive grade, please let me know before the deadlines, e.g. tell me that you are not able to submit an exercise before it is due.

1. Hand in all assignments in time
2. Do and present the final project
3. Submit your project with a report

Each of the above makes up a third of your grade, but you have to submit them all to get a positive grade.

## Additional Materials

All the materials from the course are available in the Github repo at [https://github.com/dermotte/cg-lecture-2021](https://github.com/dermotte/cg-lecture-2021).
