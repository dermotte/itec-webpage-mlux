# Introduction to Computer Graphics

This course aims to give a basic understanding of computer graphics and its role in computer games.
Topics will be a math primer, 2D and 3D graphics, shading, and hands on with practical tools.
Grading is based on (i) mandatory assignments, (ii) a final project, and (iii) a final report

## Schedule

All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below). 

1. Introduction ~ [slides](https://docs.google.com/presentation/d/16TU33dhwpADSWglb49lny0Xb94cZ2Y6vcxEKBOYqjoA/edit?usp=sharing) ~ readings:  [Sand rendering in Journey](https://www.youtube.com/watch?v=wt2yYnBRD3U)
2. p5js ~ [slides](https://docs.google.com/presentation/d/1rOsA85kH-ZCn67DA5xOoXRCaepVSVR6c6k67eKCrSnc/edit?usp=sharing) ~ readings: [p5js tutorial video](https://www.youtube.com/watch?v=8j0UDiN7my4), [Coding the Maurer Rose](https://www.youtube.com/watch?v=4uU9lZ-HSqA)
2. Math 101 ~ [slides](https://docs.google.com/presentation/d/1z50akTAnZsl9Qi1QstfqG5TZGXrGCEwxk4xzNwIFJqU/edit?usp=sharing) ~ readings: [Easing functions](https://www.youtube.com/watch?v=mr5xkf6zSzk)
3. Coordinates and Transformations ~ [slides](https://docs.google.com/presentation/d/12fGIS2yPY2yTZtLQet66zB9YlvuC0KwJdgq3iMLG9gI/edit?usp=sharing)
4. Meshes and Lights ~ [slides](https://docs.google.com/presentation/d/1T8Gf5s0hmtQizbhQ_OoN82fa6CHT22oUQo6JJPaKtXU/edit?usp=sharing)
5. Shading ~ [slides](https://docs.google.com/presentation/d/1sGvyUp1hKhJQvXcYOM3PWA6MJtyLs3nsI9qIylegOxc/edit?usp=sharing)
6. Cameras ~ [slides](https://docs.google.com/presentation/d/187vlwrIhl4uorjDM7IBYzDviZvy42K1ZmKLfO-Drfzo/edit?usp=sharing)
7. Textures ~ [slides](https://docs.google.com/presentation/d/1HXLXdL2ziDCIvrtMw5j2XJCnGH5wfGjakS_2reSNBNk/edit?usp=sharing)
7. Raytracing and Real Time Rendering ~ [slides](https://docs.google.com/presentation/d/1j_DcQKvWg5BBvCUW7OVVdkM1rhcqCa5r3KrdX06F_Eo/edit?usp=sharing)
8. Blender Workshop ~ [slides](https://docs.google.com/presentation/d/1u1ulHzLT8aoJp0fNhGSbHEk1hHoiBqSzalPqTtR35ts/edit?usp=sharing) ~ [tutorial part 1](https://youtu.be/opfrXKY0_7U) ~ [tutorial part 2](https://youtu.be/BeeEYlly8RU)

### References and Links
 1. [p5js](https://p5js.org/) - JavaScript based implementation for the first part of the course
 1. [Processing](https://processing.org/) - Java based implementation for the first part of the course
 1. [CC0 Textures](https://cc0textures.com/) - CC0 licensed high quality textures
 1. [Thingiverse](https://www.thingiverse.com/) - Database of printable 3D objects
 1. [HDRI Haven](https://hdrihaven.com/) - CC0 licensed high quality HDRIs
 1. [Kenney Assets](https://www.kenney.nl/assets) - CC0 licensed assets that can be used in Blender

### Blender Workshop
For the Blender Workshop please bring a full size keyboard and a mouse. There are several tutorials and videos that can get you into Blender:

1. [Blender Tutorials](https://www.blender.org/support/tutorials/) - Learn Blender on your own
1. [Blender Step-By-Step Videos](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6) - It's for Blender 2.8, but it's still valid in large parts
1. [Blender Guru Donut Tutorial](https://www.youtube.com/watch?v=nIoXOplUvAw&list=PLjEaoINr3zgFX8ZsChQVQsuDSjEqdWMAD)

If you have covered the basics, look out for in-depth, focused tutorials like these:
1. [Blender X-mas 3D Icon Tutorial](https://www.youtube.com/watch?v=9-WBWV_BAS4)
2. [Create 3D Animated Characters with Blender](https://www.youtube.com/watch?v=hXd4KEqrYEE)

## Exercises (Mandatory Assignments)

Please upload the exercises (just the .js or the .pde file and eventual texture or model files) to Moodle until the end of the announced day.

| ID    | Title             | Slide                                         | Deadline     |
|:------|:------------------|:----------------------------------------------|:-------------|
| EX 01 | Maurer Rose       | slide # 32 in p5js                            | Apr 3, 2023  |
| EX 02 | Vector Reflection | slide # 32 in Math 101                        | Apr 16, 2023 |
| EX 03 | LERP Game         | slide # 44 in Math 101                        | Apr 24, 2023 |
| EX 04 | Rotating Boxes    | slide # 50 in Coordinates and Transformations | May 2, 2023  |
| EX 05 | Lighting a Scene  | slide # 36 in Meshes and Lights               | May 8, 2023  |
| EX 06 | Bounded Camera    | slide # 16 in Cameras                         | May 15, 2023 |
| EX 07 | Textured Die      | slide # 29 in Textures                        | May 22, 2023 |


## Final Project
Please prepare your final project until **June 29, 2023**. Goal of the final project is to apply what you learned in the lecture in a practical way. Basic idea is to model a scene in Blender and render it. However, if this does not challenge you enough, then pick a topic and a challenge in the field of computer graphics, and get creative with a tool of your choice. This can for instance be a 3D game, a demo in a game engine, or a extensive dive into shader programming.

For the presentation (max duration 3 minutes) on June 29, 2023 make sure you can

1. show your result,
2. outline your work (contribution), and
3. discuss the lessons learned and obstacles encountered.

Please also make sure to deliver the following items in a ZIP file via upload on Moodle until **June 29, 2023**

1. Your work as source code or a link to Github, Bitbucket, etc.
2. Your result as rendering (images, movie) or executable (eg. Windows or web)
3. Your slides from the presentation
4. A PDF work report with a (i) cover page including your name and student number, (ii) a main text body of around 800 words (2 pages) detailing what you did, how you did it, and what you learned.

## How to get a positive grade?
Please make sure to fulfil the requirements to the letter. If you cannot meet the requirements for a positive grade, please let me know before the deadlines, e.g., tell me that you are not able to submit an exercise a few days before it is due.

1. Visit all classes. If not possible, contact me in advance. 
2. Submit all exercises in time
3. Do and present the final project 
4. Submit your project with a report

You have to meet all four requirements to get a positive grade. The grade itself is made in equal parts from your exercises and your final project.

Please note term paper format, code of conduct, and plagiarism policies apply as detailed below.

### Paper Format
- Orderly fulfillment of formal requirements:
    * A paper follows the [Chicago Manual of Style](https://www.chicagomanualofstyle.org/) - use the [Overleaf template](https://www.overleaf.com/latex/examples/the-chicago-citation-style-with-biblatex/pdqqrmwtdqpc) if you want a strict implementation or look at the [sample report](downloads/sample_report_introduction_CG.pdf) for reference
    * A paper is coherently formatted
    * A paper has a bibliography referencing all employed sources, including but not limited to online videos, web pages, and code repositories
- Adequate reliance on research literature:
    * Selection of citations
    * Provision of a well-formatted bibliography
-	Reliance on theoretical resources
-	Self-made observations on the discussed primary resource
-	Critical reflection on self-made observations


### Code of Conduct and Plagiarism Policies

The [University of California Berkely](https://sa.berkeley.edu/conduct/integrity/definition) offers the following definition:

```md
Plagiarism is defined as use of intellectual material produced by another person without acknowledging its source, for example:
* Wholesale copying of passages from works of others into your homework, essay, term paper, or dissertation without acknowledgment.
* Use of the views, opinions, or insights of another without acknowledgment.
* Paraphrasing of another person’s characteristic or original phraseology, metaphor, or other literary device without acknowledgment.
```

For computer science classes this includes source code, video tutorials, and web pages used to create software for the course. It is mandatory to check for license compatibility, like (i) can it be used, (ii) how to attribute, and (iii) what license is implied by including this source. It is also mandatory to cite the sources in the report. Failing to do that counts as plagiarism.

Note that the [Code of Conduct of the AAU](https://www.aau.at/en/research/research-profile/good-academic-practice/) applies. Students caught plagiarising or violation of the code of conduct will be removed from class immediately.

## Additional Materials

All the materials from the course are available in the Github repo at [https://github.com/dermotte/introduction-to-computer-graphics-lecture](https://github.com/dermotte/introduction-to-computer-graphics-lecture).
