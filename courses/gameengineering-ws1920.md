# Game Engineering

The goal of this course is to introduce students to computer game engineering. This is
not necessarily a technical issue especially as game engineering involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing. 

After this course students should be able to understand the concepts and roles within a game project with the 
knowledge how a game project can be set up and run, as well as detailed knowledge on particular processes within the 
project.

The practical course associated with the lecture is taking place as a game jam. Find more information on that on the [game jam website](http://itec.aau.at/gamejam/).

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/1BAOjEGIwt_MYo6W5BUwW8ri4W2iPEsExilDyPDwVKL4/edit?usp=sharing)
2. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/1J8_Uzi3gorucudW2RRC4-AOb4kERXxkpo5Gn56hvSe4/edit?usp=sharing)
3. Game Projects: Documents and Pitches ~ (see above slides)
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/1M-qWweZWQaWTizO2gTmlh67TMYqTx2qz4QndoON3p40/edit?usp=sharing)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1sUtp8AScdUL54yYwumfHRyhGN61grrMzX11BXr9FrcY/edit?usp=sharing)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1cLfP_uRhhaBEwc1l4CAedINE2rdubVblzy1EFzjgWUg/edit?usp=sharing)
7. Playtests ~ [slides](https://docs.google.com/presentation/d/1OraU5llPgUhO1mk6tr9p8clDjGNa4D3iNcFLM4YIrEw/edit?usp=sharing)
8. Sound & Interactive Music ~ [slides](https://docs.google.com/presentation/d/1MIeaGHS4NlLygQDzP8LCW3kJvzLROx4Mq29xUsUc9c0/edit?usp=sharing) & [introduction to synths](https://docs.google.com/presentation/d/1HSTeVkzTyy855uZHw2kYrs-KPNwIIG8rYDHek4GkAh8/edit?usp=sharing)
6. Hands On with Game Engines ~  [slides](https://docs.google.com/presentation/d/1ubgNptQgfLNiSeIY2fxfBOCr_hff40KBO8-S8fa3B0w/edit?usp=sharing)

## Modalities 

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The exam will be in written 
form, take 50 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://drive.google.com/open?id=1PF0CIe6nT5-dmRtOhRKg5nrlPFZfr6ikoH1xQHE22iU). The exam will take place at the beginning of the course on Jan 27, 2020 (10:00, S.1.42).

## Links

- [Hour of Code](https://code.org)
- [Scratch](https://mit.scratch.edu)
- [Google Blockly](https://developers.google.com/blockly/)