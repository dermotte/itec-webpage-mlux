<h1>Text documents</h1>
<p> For all text documents authors have to ensure they are readable for all participants. For non editable text the PDF
    format is preferred, for editable and formatted text either Microsoft Word (.docx) format or the most recent OpenOffice
    format (.odf). For README files use markdown or plain text. </p>
<h1>Emails</h1>
<p> For emails I recommend to: </p>
<ul>
    <li>provide a meaningful subject line and be short and concise</li>
    <li>send them in plain text (only use <abbr title="HyperText  Markup  Language">HTML</abbr>, RTF, or PDF when necessary)
    </li>
    <li>send them with a minimum attachment size (e.g. resize images, use the "reduce size" functions of PDF generators
        and text editors, etc.)
    </li>
    <li>use Dropbox, OneDrive, Google Drive, or any other service to send files larger than 5 MB</li>
    <li>follow the <a href="http://tools.ietf.org/html/rfc1855">netiquette</a> guidelines, ie. by adding a greeting
    </li>
</ul>
<h1>Submitting work</h1>
<div><p> If you submit software, text, images or any other work to me make sure to </p>
    <ul>
        <li>include the name and student id number of every participant</li>
        <li>you have the license / right to include images, text, audio, source code, or video from 3rd parties (check
            licenses)
        </li>
        <li>re-read before you sent and make sure it's the final version.</li>
    </ul>
</div>
<h1>Submitting texts like theses or reports</h1>
<div><p> Follow the rules of scientific publications. There are so many sources on how to structure and
  write scientific papers out there, every major publishers provides one. Find one and apply it. Specifically:</p>
    <ul>
        <li> pick a format and structure you text properly. </li>
        <li> cite properly! Plagiarism immediately leads to a grade 5 (failed)</li>
        <li> use automated typo and grammar checks, e.g. Grammarly. </li>
        <li> re-read before you sent and make sure it's the final version.</li>
    </ul>
</div>
<h1>Code of Conduct and Plagiarism Policies</h1>
<div>
    <p>The University of California Berkely offers the following definition:</p>

    <p>Plagiarism is defined as use of intellectual material produced by another person without acknowledging its source, for example:</p>
    <ul>
        <li>Wholesale copying of passages from works of others into your homework, essay, term paper, or dissertation without acknowledgment.</li>
        <li>Use of the views, opinions, or insights of another without acknowledgment.</li>
        <li>Paraphrasing of another person’s characteristic or original phraseology, metaphor, or other literary device without acknowledgment.</li>
    </ul>
    <p>
    For computer science classes this includes source code, video tutorials, and web pages used to create software for the course. It is mandatory to check for license compatibility, like (i) can it be used, (ii) how to attribute, and (iii) what license is implied by including this source. It is also mandatory to cite the sources in the report. Failing to do that counts as plagiarism.</p>

    <p>Note that the Code of Conduct of the AAU applies. Students caught plagiarising or violation of the code of conduct will fail the course and will be removed from class immediately.</p>
</div>
