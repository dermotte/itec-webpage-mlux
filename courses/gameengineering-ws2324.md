# Game Engineering

The goal of this course is to introduce students to computer game engineering. Game engineering is not necessarily a technical issue, especially as game engineering involves mostly non-technical work like creative audio, 3D and visual design, product management and planning, storyboarding & writing, and testing.

After this course, students should be able to understand the concepts and roles within a game project with the understanding of how a game project can be set up and run, and detailed knowledge of particular processes within the project.

The practical course associated with the lecture is taking place as a game jam on Dec 15-17, 2023. Find more information on the [game jam website](http://itec.aau.at/gamejam/) later in the semester. 

## Online Lectures

Due to the international nature of the Game Studies and Engineering program and the limited availability of lecture rooms, we'll hold roughly 50% of the lecture online on Twitch.tv. Please join the classes marked online at [https://twitch.tv/dermotte_](https://twitch.tv/dermotte_). Find the recordings of the lectures at [YouTube](https://www.youtube.com/playlist?list=PLkb7TymgoWW53Rb_3UoPN9JRkrGKaDYb3)

## Topics

1. Introduction & Computer Game History ~ [slides](https://docs.google.com/presentation/d/18-_bVR7e65YBF-LQHCDtW46vuyl5BXePZRZtiDyuHsQ/edit?usp=sharing), 
2. Game Projects: Processes and Roles ~ [slides](https://docs.google.com/presentation/d/1mVqorLZSDn74_K9ygwJxFFSXbXUd2LqhmQtQQDY9oQQ/edit?usp=sharing)
4. Prototyping ~ [slides](https://docs.google.com/presentation/d/1OSdi5o1bLZvK8ccIt4BcldbJP9RPYEdif74wabqeOTM/edit?usp=sharing)
5. Introduction to Programming ~ [slides](https://docs.google.com/presentation/d/1Tbi9HP2peh6-DllfjVsEgcruwk0gtOT4v9JWBUYyohI/edit?usp=sharing)
6. Basic Concepts in Game Programming ~ [slides](https://docs.google.com/presentation/d/1_cvN6V3mIUDJbQ9SEjsSi3aQlAH6mrwPVG_F8bs4Ya8/edit?usp=sharing)
7. Playtests and Evaluation ~ [slides](https://docs.google.com/presentation/d/1FdDcMIUy8-3DZB2AcyBA-GMXRcj9zIETUyydZjt4tQ0/edit?usp=sharing) and [slides](https://docs.google.com/presentation/d/1Efv89vdU33qc9K5X0k0So-ZEyiN5DdLdaq3Il0WSSEA/edit?usp=sharing)
8. Game Engineering and the Scientific Method ~ [slides](https://docs.google.com/presentation/d/1aFxLLl0yOukY02tctyYq4pL4FI_V0N-aiKnSFkQrg-Y/edit?usp=sharing)
9. Animation ~ [slides](https://docs.google.com/presentation/d/1oygbrxL5EtAw7UqUXZHgsaikR7QOqEge0-riEm8ChT8/edit?usp=sharing)
10. Sound & Interactive Music ~ [slides](https://docs.google.com/presentation/d/1NRN6cwdliX3DClAIohR8ptrFpZkTpyqXVbFdDLCBEUU/edit?usp=sharing)
11. Tools & Tool Chain ~ [slides](https://docs.google.com/presentation/d/10lV3tA5JmZwkPMwybWaDCFHe_Ouz8vUsilemrsM8N7A/edit?usp=sharing)

## Modalities

This course is a _Vorlesung_, meaning that the grade is awarded after a final examination. The exam will take 60 minutes of your time and will focus on the topics discussed in the course. A [list of possible exam questions is available here](https://docs.google.com/document/d/1mvdgXJl7ckqvTvY6unqEGGiHF7bwgz4frzoh4uxip9o/edit?usp=sharing).

## Links
Here are some links to dive further into the topics of the course.

### Programming
Getting startetd with programming without prior knowledge ..
- [Hour of Code](https://code.org) - Learn to program with a visual programming lecture
- [Scratch](https://mit.scratch.edu) - Use the same visual programming lecture to create interactive programs and games

### Prototyping
Simple engines for sketching out ideas.
- [Twine](https://twinery.org/) - An open-source tool for telling interactive, nonlinear stories.
- [Ren'Py](https://www.renpy.org/) - A visual novel engine

### Informative and fun videos
You want to know more? Here's a lot of material that goes beyond the course.
- [Extra Credits](https://www.youtube.com/@extracredits) - Covers many basic aspects of game dev
- [GDC Vault](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ) - Full length presentations from GDC


## General Course Information
Please note term paper format, code of conduct, AI use, and plagiarism policies apply to all courses, where applicable. Not following the required format and policies will impact your grade.

### Paper Format
- Orderly fulfillment of formal requirements:
    * A paper follows the [Chicago Manual of Style](https://www.chicagomanualofstyle.org/) - use the [Overleaf template](https://www.overleaf.com/latex/examples/the-chicago-citation-style-with-biblatex/pdqqrmwtdqpc) if you want a strict implementation.
    * A paper is coherently formatted and has been checked for typos and grammatical errors (you can use Grammarly for instance to do that)
    * A paper has a bibliography referencing all employed sources, including but not limited to online videos, web pages, and code repositories
- Adequate reliance on research literature:
    * Selection of citations
    * Provision of a well-formatted bibliography
-	Reliance on theoretical resources
-	Self-made observations on the discussed primary resource
-	Critical reflection on self-made observations


### AI Use in this Course
While students are encouraged to use any tools you can find as a sparring partner for coding or resource for learning, the course goal is to be able to program the exercises and the final project individually. If there is a suspicion that parts of the source code or texts were written by an AI tool, you will be invited to a clarifying discussion, which can also influence the grade. In addition, the rules for plagiarism (with "another person" being the AI model) also apply.

### Code of Conduct and Plagiarism Policies

The [University of California Berkely](https://sa.berkeley.edu/conduct/integrity/definition) offers the following definition:

```md
Plagiarism is defined as use of intellectual material produced by another person without acknowledging its source, for example:
* Wholesale copying of passages from works of others into your homework, essay, term paper, or dissertation without acknowledgment.
* Use of the views, opinions, or insights of another without acknowledgment.
* Paraphrasing of another person’s characteristic or original phraseology, metaphor, or other literary device without acknowledgment.
```

For computer science classes this includes source code, video tutorials, and web pages used to create software for the course. It is mandatory to check for license compatibility, like (i) can it be used, (ii) how to attribute, and (iii) what license is implied by including this source. It is also mandatory to cite the sources in the report. Failing to do that counts as plagiarism.

Note that the [Code of Conduct of the AAU](https://www.aau.at/en/research/research-profile/good-academic-practice/) applies. Students caught plagiarising or violation of the code of conduct will be removed from class immediately.
