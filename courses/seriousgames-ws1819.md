# Non Entertainment Games

The goal of this course is to introduce students to games that are mainly created for purposes other than entertainment. This includes but is not limited to serious games, educational games, adware games, fitness games, and propaganda games. 

## Schedule

All courses start c.t. if not otherwise noted.
 
| Day    | Time   | Place  | Topic | Links |
|:------|:------|:------|:---|:---|
| Mo, 08.10. | 14-16 | E.2.69 | Introduction & GWAPs | [slides](https://docs.google.com/presentation/d/1R7pIMHk-4k5vIf0NyJ1R8zbYzN-HOr17o3VgjmCIazI/edit?usp=sharing) |
| Mo, 22.10. | 14-16 | E.2.69 | Flow Assessment      | [slides](https://docs.google.com/presentation/d/1fNAXh3DIMpFB3BTP2zctP07e6gm-fi-BslagnKrvsEs/edit?usp=sharing) |
| Tu, 08.11. | 9:30-18:00  | inspire Lab | Management Game | [GameFlow model sheet](https://docs.google.com/spreadsheets/d/1IF62jV8xnMin_Hxms6vFAGJw2Sv18xZ5s9gH99GGNQ4/edit?usp=sharing) |
| Fr, 09.11. | 9:00-15:30  | inspire Lab | Management Game | [eGameFlow questionnaire](https://docs.google.com/spreadsheets/d/1j8XcFhtz10hv5ilMs9tUEUhxK_Oz0Pu09mRSc7pm9Fg/edit?usp=sharing) |
| Mo, ??.11. | 14-16 | E.2.69 | Discussion and flow assessment | tba. |
| Mo, ??.01. | 14-16 | E.2.69 | Presentations | tba. |
| Mo, ??.01. | 14-16 | E.2.69 | Presentations | tba. |


## Modalities 

This course is a _Seminar_, meaning that the grade is based on active participation in the seminar as well as a final seminar presentation & written work. There will be a blocked lecture on Nov 8-9 2018, where the group plays and discusses a game of that type. 
