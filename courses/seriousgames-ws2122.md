# Non Entertainment Games

This course aims to introduce students to games that are mainly created for purposes other than entertainment. This includes but is not limited to serious games, educational games, adware games, fitness games, and propaganda games.

## Online, but How?

Due to the restrictions necessary to prevent the spread of COVID-19 we are going to provide all course materials in Game Studies and Engineering [online](https://www.twitch.tv/dermotte_). Please make sure to join the GSE Discord Server to stay up to date [https://discord.gg/YnX6AKG](https://discord.gg/YnX6AKG)

Recordings are available, either on Twitch, on [YouTube](https://youtube.com/playlist?list=PLkb7TymgoWW58SjbXnlGVssXZaUqnwPT-), or [for download](https://www2.itec.aau.at/owncloud/index.php/s/CJqPnzKlNDEVIzU).

## Schedule

All courses start s.t. if not otherwise noted.

| Day    | Time   | Place  | Topic | Links |
|:------|:------|:------|:---|:---|
| Th, 14.10. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | Introduction | [slides](https://docs.google.com/presentation/d/1ydAzvHSV8VJeYgKgpEhTDGy-SkdYfGk4pKkUbCTU7tA/edit?usp=sharing) |
| Th, 21.10. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | GWAPs & topic distribution | [slides](https://docs.google.com/presentation/d/1aH2kY-IKnBcNZhOTmlfABbgz1957eB1seOONFoaBTys/edit?usp=sharing) |
| Th, 28.10. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | Flow Assessment for Games | [slides](https://docs.google.com/presentation/d/1Mju5xF_qM7Yh8446HuWij4jLE685VtrzUvp6JXCrl-4/edit?usp=sharing) |
| Th, 04.11. | 10:00-11:30 | [online](https://www.twitch.tv/dermotte_) | Serious Games | tba. |
| Th, 11.11. | 10:00-11:30 | Discord | Intermediate presentations | none |
| Th, 09.12. | 10:00-11:30 | Discord | Q&A (optional: drop by if you have questions) | none |
| Th, 16.12. | 10:00-11:30 | Discord | Q&A (optional: drop by if you have questions) | none |
| Th, 13.01. | 10:00-11:30 | Discord | Q&A (optional: drop by if you have questions) | none |
| Th, 20.01. | tba | Discord | Student Presentations | tba. |
| Th, 27.01. | tba | Discord | Student Presentations | tba. |




## Modalities

This course is a _Seminar_, meaning that the grade is based on active participation in the seminar as well as a final seminar presentation & written work.

A positive grade can be achieved by reaching at least Level 3 in the [Experience System](https://docs.google.com/spreadsheets/d/1srVIzdRr6JaOw0sJi21RAxNq0mkq1CVAC_B3hVdB8Bw/edit?usp=sharing) for the class. **Mandatory tasks** are

* Giving the intermediate presentation on Nov 11, 2021. [sample presentation](https://docs.google.com/presentation/d/1f3AMg8tub5xXdzM-j3lFD59sV3q4GcOWhnawRX7ZjVA/edit?usp=sharing) (3 minutes presentation and 2 minutes of Q&A)
* Giving a presentation at the end of Jan 2022: 10 minutes of presentation and 5 minutes of discussion each on Discord.
* Handing in the seminar report (1500 words) until Feb 28, 2022. [sample seminar report](https://docs.google.com/document/d/1AEOIGAU8Z4GZw1t0eU2FHXjkZdqlLkfDLG5Uz9juGr8/edit?usp=sharing)

Note that doing the mandatory tasks results nearly in a positive grade, but you will have to do optional tasks or complete achievements, like handing in early, too.

### Extra Curricular Activities
A common optional task is participating in or organizing an extra curricular activity. It's self-organized, so a student proposes an activity, involving a non-entertainment game to the teacher. After being signed off, the organizer proposes the activity publicly (and early enough) on Discord in the Non-Entertainment Games channel. Students then can participate. After the activity the organizer sends a short report to the teacher, including the names of the people who participated and a very short summary. Each student can get at maximum one organizer achievement (100 XP) but multiple participation achievements (25 XP).

An example would be to propose a fitness game, like Zombies, Run! and people do a running training with the app and then reflect on it in a group discussion.    

## Topics

[Topics and distribution thereof](https://docs.google.com/spreadsheets/d/1tYmzePLqdnFdExwHzbvhVG9g6W3guWSTI19a8WnNLcs/edit?usp=sharing)
