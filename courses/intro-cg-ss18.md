# Introduction to Computer Graphics

This course aims to give a basic understanding of computer graphics and its role in computer games. 
Topics will be a math primer, 2D and 3D graphics, shading, and hands on with practical tools. 
Grading is based on mandatory assignments as well as a final projects done as team work. 

## Schedule

| Day    | Time   | Place  | Topic | Readings |
|:------|:------|:------|:---|:---|
| Do, 08.3.  | 14-16 | E.2.42 | Introduction ~ [slides](https://docs.google.com/presentation/d/1q1y3qxGkXmJpLa2Uwhsb-rfCij-SnP70uXgcgadmh4w/edit?usp=sharing)|  [Sand rendering in Journey](https://www.youtube.com/watch?v=wt2yYnBRD3U) |
| Do, 15.3.  | 14-16 | E.2.42 | Basics Maths for Computer Graphics ~ [slides](https://docs.google.com/presentation/d/1epmUN0KnNnhEVA6m21NgCtrXDjsFQiMOoldInaMfTOk/edit?usp=sharing) | [Easing functions](https://www.youtube.com/watch?v=mr5xkf6zSzk) |
| Do, 22.3.  | 14-16 | E.2.42 | Coordinate Systems and Matrices ~ [slides](https://docs.google.com/presentation/d/1dvyKToHcUBUxxYO-1n-YmHMY7LNO8Nohtvh5FZA88Z8/edit?usp=sharing)| none |
| Do, 19.4.  | 14-16 | E.2.42 | Meshes and Lights ~ [slides](https://docs.google.com/presentation/d/1T4soSeuUk4MoeXcMwwJGgjD4k4eQ7Um_GYzN5lxnTFw/edit?usp=sharing) | [Three Point Lighting](https://youtu.be/Bmme1slWdm4) |
| Do, 26.4.  | 14-16 | E.2.42 | Shading and Cameras ~ [slides](https://docs.google.com/presentation/d/1rzhQPXEbsvhpjSt6XMh-sCL3yrxVY3xYM10MljxFcqQ/edit?usp=sharing) | [Stable Elasticity from Pixar](http://graphics.pixar.com/library/StableElasticity/) |
| Do, 03.5.  | 14-16 | E.2.42 | Textures ~ [slides](https://docs.google.com/presentation/d/1B2ExxTG6yD3Ki23-1eFIBBTWsv0uR4XhlXT9CRtKaU8/edit?usp=sharing) | [Three.js Intro](https://youtu.be/6eLl8yQnxHQ) |
| Do, 24.5.  | 14-16 | E.2.42 | Real Time Rendering, GPUs, Raytracing ~ [slides](https://docs.google.com/presentation/d/1ovIzbHnxBnj2hNhI5aBjEt8R-iX-fmYHriUz_a27jm4/edit?usp=sharing) and  [PBR / Karl-Heinz Pirolt](https://docs.google.com/presentation/d/1dOp3LH48A5K93jytT7nINloo3ar1r5BezyNRYE6FQeo/edit?usp=sharing) | [Pixar Renderman Showreel](https://www.youtube.com/watch?v=E3vPJybuG9s)|
| Fr, 25.5.  | 10-14 | E.2.37 | Blender Fundamentals (see also [1], videos 1-11) ~ [slides](https://docs.google.com/presentation/d/1M4zHv8teRI_8_QSb4tvwg2tNp_cybz8O4B6r7Ff2g_Q/edit?usp=sharing) | [Low Poly Illustrations in Blender](https://cgi.tutsplus.com/tutorials/secrets-to-creating-low-poly-illustrations-in-blender--cg-31770) |
| Tu, 29.5.  | 09-13 | M.0.22 | Blender Modeling | [Low Poly Animals Tutorial](https://www.youtube.com/watch?v=JjW6r10Mlqs) |
| Do, 21.6.  | 14-16 | E.2.42 | Blender -> Unity | tba. |
| Do, 28.6.  | 14-16 | E.2.42 | Presentations  | tba. |

### References and Links
 1. [Blender Tutorials](https://www.youtube.com/playlist?list=PLa1F2ddGya_8V90Kd5eC5PeBjySbXWGK1)
 1. [Processing](https://processing.org/)

## Exercises

Please send the exercises to M. Lux until the end of the announced day.

| ID | Title | Slide | Deadline | Example Solution |
|:---|:---|:---|:---|:---|
| EX 02-01 | Vector Reflection | Slide 30 in [Lecture 02](https://docs.google.com/presentation/d/1epmUN0KnNnhEVA6m21NgCtrXDjsFQiMOoldInaMfTOk/edit?usp=sharing) | Apr 19, 2018 | ... |
| EX 02-02 | The Lerp Game | Slide 42 in [Lecture 02](https://docs.google.com/presentation/d/1epmUN0KnNnhEVA6m21NgCtrXDjsFQiMOoldInaMfTOk/edit?usp=sharing) | Apr 19, 2018 | ... |
| EX 03-01 | Two rotating boxes rotating | Slide 40 in [Lecture 03](https://docs.google.com/presentation/d/1dvyKToHcUBUxxYO-1n-YmHMY7LNO8Nohtvh5FZA88Z8/edit?usp=sharing) | Apr 19, 2018 | ... |
| EX 04-01 | Rotating lights | Slide 35 in [Lecture 04](https://docs.google.com/presentation/d/1T4soSeuUk4MoeXcMwwJGgjD4k4eQ7Um_GYzN5lxnTFw/edit?usp=sharing) | May 03, 2018 | |
| EX 05-01 | Bounded Camera | Slide 41 in [Lecture 05](https://docs.google.com/presentation/d/1rzhQPXEbsvhpjSt6XMh-sCL3yrxVY3xYM10MljxFcqQ/edit?usp=sharing) | May 24, 2018 | |
| EX 06-01 | Texturing Dice | Slide 24 in [Lecture 6](https://docs.google.com/presentation/d/1B2ExxTG6yD3Ki23-1eFIBBTWsv0uR4XhlXT9CRtKaU8/edit?usp=sharing) | Jun 10, 2018 |  |


## Materials

All the materials from the course are available in the Github repo at [https://github.com/dermotte/cg-lecture-2018](https://github.com/dermotte/cg-lecture-2018).

## Final Projects
Here are some of the final projects to try out and take a look at:

* https://www.youtube.com/watch?v=EfL_rawMjz4 - Rendering of a Blender Scene
* https://www.youtube.com/watch?v=1bePko3CSyw - Rendering of a Blender Scene
* https://geist-191.itch.io/shadow-play - 3D Game made in Unity
* https://dr4g0nsoul.itch.io/vis-vis - 3D Game made in Unreal
* https://drive.google.com/open?id=1LE__G4Nr1ZQbhhREgmYeZU2t8IifAc6a - Renderings from a Voxel Editor