# Introduction to Computer Game Production 

The goal of this course is to introduce students to computer game production. This is
not only a technical issue, especially as game production involves non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing.

The course has already taken place in previous years at Klagenfurt University, so you might take a look at the
[2015 web page](?id=courses/gamesss15) or the [2014 web page](?id=courses/gamesss14). 

# Schedule


| Day        | Time   | Place  | Topic | Reading |
|:------|:------|:------|:---|:---|
| Tu, 22.11. | 10-12 | na. | Introduction and motivation  | [Video - Grounded: The Making of The Last of Us](https://www.youtube.com/watch?v=yH5MgEbBOps) |
| Tu, 22.11. | 14-16 | na. | Game Development - Game Design (selected topics) and Game Loop  | [Life is Strange Case Study (GDC 2016)](http://www.gdcvault.com/play/1023468/-Life-is-Strange-Case) |
| Fr, 25.11. | 10-12 | na. | Game Development - 2D Graphics | [Postmortem, explained by a video](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/postmortem.mp4) |
| Fr, 25.11. | 14-16 | na. | Game Development - Input, Sound &amp; Physics | tba. |




# Slides

1. [Introduction, 611 KB](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/games-ws16-01-introduction-motivation.pdf)
1. [Software Design, 376 KB](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/games-ws16-02-game-software-design.pdf)
1. [Game Loop, 522 KB](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/games-ws16-03-game-dev-game-loop.pdf)
1. [2D Graphics, 1.351 KB](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/games-ws16-04-game-dev-2d-graphics.pdf)
1. [Input and Sound, 510 KB](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/games-ws16-05-game-dev-input_sound.pdf)
1. [Game Physics, 279 KB](http://www.itec.uni-klu.ac.at/~mlux/teaching/games16toulouse/games-ws16-06-game-dev-physics.pdf)