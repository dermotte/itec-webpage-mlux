# Introduction to Computer Graphics

This course aims to give a basic understanding of computer graphics and its role in computer games.
Topics will be a math primer, 2D and 3D graphics, shading, and hands on with practical tools.
Grading is based on (i) mandatory assignments, (ii) a final project, and (iii) a final report

## Schedule

All courses start s.t. in [Klagenfurt local time](https://www.timeanddate.com/worldclock/austria/klagenfurt) if not otherwise noted. Slides are available online (see below). 

1. Introduction ~ [slides](https://docs.google.com/presentation/d/1184ki-YXbEt8ZQ7vGIn4s-ZvhOUcdgaVdcKee0AvR5c/edit?usp=sharing)
2. p5js 
2. Math 101 
3. Coordinates and Transformations 
4. Meshes and Lights 
5. Shading 
6. Cameras 
7. Textures 
7. Raytracing and Real Time Rendering 
8. Blender Workshop 

### References and Links
 1. [p5js](https://p5js.org/) - JavaScript based implementation for the first part of the course
 1. [CC0 Textures](https://cc0textures.com/) - CC0 licensed high quality textures
 1. [Thingiverse](https://www.thingiverse.com/) - Database of printable 3D objects
 1. [HDRI Haven](https://hdrihaven.com/) - CC0 licensed high quality HDRIs
 1. [Kenney Assets](https://www.kenney.nl/assets) - CC0 licensed assets that can be used in Blender

### Blender Workshop
For the Blender Workshop please bring a full size keyboard and a mouse. There are several tutorials and videos that can get you into Blender in a self-guided manner:

1. [Blender Tutorials](https://www.blender.org/support/tutorials/) - Learn Blender on your own
1. [Blender Step-By-Step Videos](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6) - It's for Blender 2.8, but it's still valid in large parts
1. [Blender Guru Donut Tutorial](https://www.youtube.com/watch?v=nIoXOplUvAw&list=PLjEaoINr3zgFX8ZsChQVQsuDSjEqdWMAD) - Everybody knows that one.

If you have covered the basics, look out for in-depth, focused tutorials like these:
1. [Blender X-mas 3D Icon Tutorial](https://www.youtube.com/watch?v=9-WBWV_BAS4)
2. [Create 3D Animated Characters with Blender](https://www.youtube.com/watch?v=hXd4KEqrYEE)

## Exercises (Mandatory Assignments)

Please upload the exercises (exported zip from the p5js editor) to Moodle until the end of the announced day. The use of [p5js](https://p5js.org/) is mandatory.

| ID    | Title             | Slide                                                                                                       | Deadline |
|:------|:------------------|:--- |:---------|
| EX 01 | Maurer Rose       |   | Mar 26  | 
| EX 02 | Vector Reflection |   | Apr 02  |
| EX 03 | LERP Game         |   | Apr 09  |
| EX 04 | Rotating Boxes    |   | May 07  |
| EX 05 | Lighting a Scene  |   | May 14  |
| EX 06 | Bounded Camera    |   | May 21  |
| EX 07 | Textured Die      |   | Jun 04  |


## Final Project
Please prepare your final project until **June 25, 2025**. Goal of the final project is to apply what you learned in the lecture in a practical way. Basic idea is to model a scene in Blender and render it. 

For the presentation (max duration 3 minutes) on June 26 2025, make sure you can

1. show your result,
2. outline your work (contribution), and
3. discuss the lessons learned and obstacles encountered.

Please also make sure to deliver the following items in a ZIP file via upload on Moodle until **June 25, 2025**

1. Your work as source code or a link to Github, Bitbucket, etc.
2. Your result as rendering (images, movie) or executable (eg. Windows or web)
3. Your slides from the presentation
4. A PDF work report with a (i) cover page including your name and student number, (ii) a main text body of around 800 words (2 pages) detailing what you did, how you did it, and what you learned.

The scope of the final project is defined as follows:
- Model three objects, e.g., by extrusion
- Add material to the objects (shaders, textures, etc.)
- Populate a scene with your objects and optional imported ones
- Light the scene
- Render a picture as well a short animation

Constraints to the project:
- You have to do the final project in Blender
- You have to use low poly style
- You have to use three or more light sources
- You have to use Eevee as a renderer

## How to get a positive grade?
Please make sure to fulfil the requirements to the letter. If you cannot meet the requirements for a positive grade, please let me know before the deadlines, e.g., tell me that you are not able to submit an exercise a few days before it is due.

1. Visit all classes. If not possible, contact me in advance. 
2. Submit all exercises in time.
3. Do and present the final project.
4. Submit your project with a report.

You have to meet all four requirements to get a positive grade. The grade itself is made in equal parts from your exercises and your final project.

Please note term paper format, code of conduct, and plagiarism policies apply as detailed below.

### Paper Format
- Orderly fulfillment of formal requirements:
    * A paper follows the [Chicago Manual of Style](https://www.chicagomanualofstyle.org/) - use the [Overleaf template](https://www.overleaf.com/latex/examples/the-chicago-citation-style-with-biblatex/pdqqrmwtdqpc) if you want a strict implementation or look at the [sample report](downloads/sample_report_introduction_CG.pdf) for reference
    * A paper is coherently formatted
    * A paper has a bibliography referencing all employed sources, including but not limited to online videos, web pages, and code repositories
- Adequate reliance on research literature:
    * Selection of citations
    * Provision of a well-formatted bibliography
-	Reliance on theoretical resources
-	Self-made observations on the discussed primary resource
-	Critical reflection on self-made observations


### Code of Conduct and Plagiarism Policies

The [University of California Berkely](https://sa.berkeley.edu/conduct/integrity/definition) offers the following definition:

```md
Plagiarism is defined as use of intellectual material produced by another person without acknowledging its source, for example:
* Wholesale copying of passages from works of others into your homework, essay, term paper, or dissertation without acknowledgment.
* Use of the views, opinions, or insights of another without acknowledgment.
* Paraphrasing of another person’s characteristic or original phraseology, metaphor, or other literary device without acknowledgment.
```

For computer science classes this includes source code, video tutorials, and web pages used to create software for the course. It is mandatory to check for license compatibility, like (i) can it be used, (ii) how to attribute, and (iii) what license is implied by including this source. It is also mandatory to cite the sources in the report. Failing to do that counts as plagiarism.

Note that the [Code of Conduct of the AAU](https://www.aau.at/en/research/research-profile/good-academic-practice/) applies. Students caught plagiarising or violation of the code of conduct will be removed from class immediately.

## Additional Materials

All the materials from the course are available in the Github repo at [https://github.com/dermotte/introduction-to-computer-graphics-lecture](https://github.com/dermotte/introduction-to-computer-graphics-lecture).
