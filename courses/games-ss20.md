# Interaktive Multimediasysteme B (Computer Games) 

The goal of this course is to introduce students to computer game production. This is
not necessarily a technical issue especially as game production involves mostly non-technical work like
creative audio, 3D and visual design, product management and planning, story boarding &amp; writing, and
testing.
The course has already taken place in previous years, so you might take a look at the
[2019 web page](?id=courses/games-ss19.md). The lecture is given in German, 
but slides and homepage are written in English. For assignments you can choose between German and English language.

## Modalities
The course aims to teach in a practical as well as in a theoretical way. Throughout the
lecture you will have (i) readings, (ii) practical assignments, and (iii) a final game project. The readings
are specific to the lecture topic and will be referenced from the web page. Make sure you read (or watch)
them. Questions might pop up in the final multiple choice test. The final grade is based on the (i)
assignments as well as the final project and a (ii) final multiple choice test.

* Readings - For the course it's mandatory to read (or view, in case of videos) the readings. They will be announced throughout the course in the schedule table.
* Practical assignment - This year will feature one practical assignments prior to the final project.
* Written exam - A final, short, multiple choice exam on the course topics at the end of the course.     
* Final project - Create a game in a group, more information see below.

## Schedule

All courses start c.t. if not otherwise noted. Slides are available online (sse below). Due to the situation around CoViD-19 all besides the first lecture take place online. After the online courses, video lectures are available. 

After the course you can find me in the _Lectures Q&A_ audio channel in the [GSE Discord Server](https://discord.gg/YnX6AKG).
 
| Day    | Time   | Place  | Topic | Readings |
|:------|:------|:------|:---|:---|
| Mo, 09.3.  | 14-16 | S.2.69 | Introduction and motivation ~ [slides](https://docs.google.com/presentation/d/1VpFDyQiQXTBgNcgLKa-Cer9dgrJoIq68PKaNfyqHqs8/edit?usp=sharing) | [Game - AI Dungeon](https://aidungeon.io/) |
| [video lecture](https://youtu.be/-FjLlvR_UEw)  | n.a. | n.a. | Business of Games ~ [slides](https://docs.google.com/presentation/d/1i71LWmPZZKIBcihmquldxcPP7xK6bPxJ5vlZxi26VDs/edit?usp=sharing), [video lecture](https://youtu.be/-FjLlvR_UEw) | [Video - Free to Play](http://store.steampowered.com/app/245550/?snr=1_7_7_151_150_1) |
| Mo, 23.3.  | 14-16 | [video recording](https://youtu.be/sj6hJJ2-Vf8) | History of Games ~ [slides](https://docs.google.com/presentation/d/1_hzceB1xAzHbQE09edj89Oahieg7Y9sVEJ1nA8WPMes/edit?usp=sharing) | [Game - Paperclip](https://www.decisionproblem.com/paperclips/index2.html) |
| Mo, 30.3.  | 14-16 | [video recording](https://youtu.be/GmxI7y5xBfE) | Game Production and Development ~ [slides](https://docs.google.com/presentation/d/1N7iWL1mBlsL4lJEFb97OyuUQ9gw7Q3sUcz_IsBAChLk/edit?usp=sharing) | [Video - Postmortem Dying Light](https://youtu.be/UJgTboToOg8) |
| Mo, 20.4.  | 14-16 | [video recording](https://youtu.be/bMNKt_Q_KTA) | Game Design ~ [slides](https://drive.google.com/open?id=1m2nojoey8USY1DWQXKttvNhwrhoQsvoAAtGetQK6tUs) |  [Level Design Workshop: Designing Celeste](https://www.youtube.com/watch?v=4RlpMhBKNr0) |
| Mo, 27.4.  | 14-16 | [video recording](https://youtu.be/UwRAPZ7UVxM) | Game Loop ~ [slides](https://drive.google.com/open?id=1pSS7ZTGBFl0MGh1fgXllO4td3QkoJyzHuKmGQ7b50i4) |  Try 2 or more [klujam games](https://itch.io/jam/7thklujam) |
| Mo, 04.5.  | 14-16 | [video recording](https://youtu.be/9JmyIfsqPJg) | 2D Graphics ~ [slides](https://docs.google.com/presentation/d/1JBaIAuYnkG3MSqI7yMEN6tm0VjHfmXA0HvT0MsAJrAU/edit?usp=sharing) |  [Video - Juice It or Loose It](https://www.youtube.com/watch?v=Fy0aCDmgnxg) |
| Mo, 11.5.  | 14-16 |[video recording](https://youtu.be/y9z__X9_bL0) | Input and Sound ~ [slides](https://docs.google.com/presentation/d/1VTxY2ZuI0o_PqR9QOhXWaIqUwi7KviKs11o-HD5t2jU/edit?usp=sharing) |  ... |
| Mo, 18.5.  | 14-16 |[video recording](https://youtu.be/G1mpdo4OWyQ) | Introduction to 3D Graphics ~ [slides](https://docs.google.com/presentation/d/1DZ-8QEbDRNs51RCMT-PqY8CakwvFLad91JvQAid6lvQ/edit?usp=sharing) |  ... |
| Mo, 25.5.  | 14-16 |[video recording](https://youtu.be/8oSlw6mKV6I) | Game AI ~ [slides](https://docs.google.com/presentation/d/1biAVTrKxbN8ThJRe6R_4vzouhkabTbkRxzXVWcY9EcA/edit?usp=sharing) |  ... |
| Mo, 08.6.  | 14-16 |[online](https://www.twitch.tv/dermotte_) | Game Physics ~ [slides](https://docs.google.com/presentation/d/1thrZQ3dejkqdMGWUt12AKb8skbXATc_yr2EmKeIeh4g/edit?usp=sharing) |  ... |
| Mo, 15.6.  | 14-16 | [Discord](https://discord.gg/JHNgyAe)  | Student Projects: Presentations |  none |

## Game Jam
You are strongly encouraged to take part in the game jam, from Apr 24-26, 2020. You can join any group there and the game developed at the game jam can be presented as final project.

## Mandatory Exercise 01 - Pong with Löve
For the first mandatory exercise you are to create a Pong clone with [Löve2D](https://love2d.org/). It does not need to be an accurate copy (graphically or otherwise), but just let two people play a game of pong and count the points. Your Pong clone should fulfil the following requirements:

1. Submit as runnable .love file
1. Use `love.graphics.rectangle(...)` to paint the bars and the ball
1. Control by keyboard

Submit your Pong clone to Mathias Lux via e-mail until **April 19th, 2020, midnight**.

Are you not challenged enough? - Use the [Windfield physics wrapper for Löve2D](https://github.com/adnzzzzZ/windfield) to create a pong version with a physics engine in the back end. Make sure to reduce friction of the ball and set the position of the paddles in the update callback function ;)   
 
## Mandatory Exercise 02 - Fun with Godot
For the second mandatory exercise you are to create a game, where you land rocket on a small plateau, with [Godot](https://godotengine.org/).  Your game should fulfil the following requirements:

1. Use the built-in Godot physics engine with the rocket / space ship being a _RigidBody2D_ and the plateau being a _StaticBody2D_
1. Use the arrow keys to rotate and fire up the rocket
1. Let the game be similar to the example: [play in browser](http://www.itec.uni-klu.ac.at/~mlux/games/cg2020/) or [download for windows](http://www.itec.uni-klu.ac.at/~mlux/games/cg2020/example-game-windows.zip)
1. Submit as zipped Godot Project.

Submit your game to Mathias Lux via e-mail until **May 3rd, 2020, midnight**.

## Final Project
Your final project can be done in a group with up to three people. To finish the project you need to:
1. Hand in the project & the documentation
   1. Link to the project (ie. itch.io, Github, WeTransfer, ...)
   1. game concept document (1 page)
   1. post mortem (1 page)
   1. presentaiton slides
1. Present the project in the lecture on June 15, 2020 (10 minutes max.)
   1. list the group members
   1. explain the concept
   1. show the game
   1. talk about the lessons learned (post mortem)
   
The project can be a stand-alone game, a level for a game, or a mod for a game. If you attended the game jam, please present your game jam project and just create the documents and slides. 