<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tools</title>
    <link rel="icon" type="image/png" href="../favicon.png"/>

    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">

    <div class="row" style="margin-bottom: 2em">
        <div class="col-md-12">
            <h1>Tools & Web Applications</h1>
            <p>These things are provided as is. No warranties and return policies.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <a href="groups.html" class="btn btn-success btn-lg btn-block">Random Groups</a>
            <p>Create random groups from Excel and per line text lists.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="postit.html" class="btn btn-success btn-lg btn-block">Post-Its</a>
            <p>Create Post-It like text notes in the browser.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="../identity" class="btn btn-success btn-lg btn-block">Identities</a>
            <p>Fake identities based on real names ready to use.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="../pwd" class="btn btn-success btn-lg btn-block">Passwords</a>
            <p>Random password generator with approximate strength.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="timer.html" class="btn btn-success btn-lg btn-block">Countdown</a>
            <p>Simple countdown timer for presentations.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="timer/index.html" class="btn btn-success btn-lg btn-block">Interval Timer</a>
            <p>Simple interval timer for sports.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="scriptrun.html" class="btn btn-success btn-lg btn-block">Javascript in browser</a>
            <p>Simple method to run Javascript in the browser.</p>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="maze.html" class="btn btn-success btn-lg btn-block">Maze Generator</a>
            <p>Maze generator based on Prim's algorithm</p>
        </div>
    </div>
</div>

</body>
</html>
